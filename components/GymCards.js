import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Ionicons } from "@expo/vector-icons";

export default function GymCards(props) {
  return (
    <View style={styles.mainCont}>
      <View style={styles.container}>
        {/* Top Left Button */}
        <TouchableOpacity
          style={styles.barCardTopTag}
          onPress={props.onMenuPress}
        >
          <View style={styles.barCardTopTagTextWrapper}>
            <Text style={styles.barCardTopTagText}>
              {props?.data?.applicants.length} Applied
            </Text>
          </View>
        </TouchableOpacity>
        {/* Top Left Button */}
        <View style={styles.FirstRow}>
          <View style={styles.profile}>
            <Image
              style={{ width: "100%", height: "100%" }}
              source={require("../assets/profile.jpg")}
              resizeMode="cover"
            />
          </View>
          <View style={styles.Detail}>
            <Text
              style={{
                fontSize: rf(13),
                fontWeight: "700",
                color: "#E2B269",
                fontFamily: "MB",
                marginBottom: 5,
              }}
            >
              {props?.data?.title}
            </Text>
            <Text
              style={{
                fontSize: rf(10),
                fontWeight: "700",
                color: "#E2B269",
                fontFamily: "MR",
                marginVertical: 5,
              }}
            >
              {props?.data?.location}
            </Text>
            <Text
              style={{
                fontSize: rf(11),
                fontWeight: "700",
                fontFamily: "MR",
                color: "#E2B269",
                top: 10,
                width: "100%",
              }}
              numberOfLines={4}
            >
              {props?.data?.describtion}
            </Text>
          </View>
          {!props.noHeart && (
            <View style={styles.Icon}>
              <TouchableOpacity onPress={props.onHeartPress}>
                <AntDesign
                  name="heart"
                  size={rf(22)}
                  color={props.favourite ? "#E2B269" : "#222"}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={styles.SecondRow}>
          <View style={styles.date}>
            <Text
              style={{
                fontFamily: "MR",
                fontSize: rf(10),
                fontWeight: "700",
                opacity: 0.5,
              }}
            >
              Posted: {props.data?.date.toDate().toDateString()}
            </Text>
          </View>
          <View style={styles.Btn}>
            {props?.data?.hiredSomeone ? (
              <View style={styles.ApplyBtn}>
                <Text
                  style={{
                    fontFamily: "MM",
                    fontSize: rf(12),
                    fontWeight: "700",
                    color: "#fff",
                  }}
                >
                  Closed
                </Text>
              </View>
            ) : (
              <TouchableOpacity
                style={styles.ApplyBtn}
                onPress={props.onApplyClick}
              >
                <Text
                  style={{
                    fontFamily: "MM",
                    fontSize: rf(12),
                    fontWeight: "700",
                    color: "#fff",
                  }}
                >
                  {props.status == "applied"
                    ? "Applied"
                    : props.status == "apply"
                    ? "Apply Now"
                    : props.View
                    ? "View"
                    : ""}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainCont: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
    paddingVertical: 10,
    width: wp("100%"),
  },
  container: {
    width: "95%",
    height: hp("20%"),
    backgroundColor: "#CDCDCD",
    borderRadius: 14,
    marginHorizontal: "2.5%",
    overflow: "hidden",
  },
  FirstRow: {
    width: "100%",
    height: "70%",
    backgroundColor: "#fff",
    borderRadius: 14,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  profile: {
    width: hp("8%"),
    height: hp("8%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Detail: {
    width: "65%",
    height: "50%",
    justifyContent: "space-evenly",
    paddingHorizontal: 7,
  },
  Icon: {
    width: "22%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  SecondRow: {
    width: "100%",
    height: "35%",
    flexDirection: "row",
  },
  date: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Btn: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  ApplyBtn: {
    width: "75%",
    height: "52%",
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
  },
  barCardTopTag: {
    width: wp("33%"),
    height: hp("3%"),
    borderRadius: wp("50%"),
    borderBottomLeftRadius: wp("5%"),
    backgroundColor: "#F5CA48",
    transform: [{ rotateX: "-180deg" }],
    right: -wp("5%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 99999,
  },
  barCardTopTagTextWrapper: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  barCardTopTagText: {
    fontSize: rf(11),
    fontWeight: "bold",
    transform: [{ rotateX: "-180deg" }],
    fontFamily: "MM",
  },
});
