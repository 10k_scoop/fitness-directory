import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo, AntDesign, FontAwesome5 } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onHomeClick}>
        <Entypo name="home" size={rf(20)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity>
        <FontAwesome5 name="dumbbell" size={rf(33)} color="#D4A35A" />
      </TouchableOpacity>

      <TouchableOpacity onPress={props.onLogoutPress}>
        <AntDesign name="logout" size={rf(20)} color="#D4A35A" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%") + StatusBar.currentHeight,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    paddingVertical: "3%",
    paddingHorizontal: "5%",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
  Icon: {
    width: "17%",
    height: "100%",
    alignItems: "flex-end",
  },
});
