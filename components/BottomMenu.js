import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  FontAwesome,
  FontAwesome5,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";

export default function BottomMenu(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => props.navigation.navigate("Favourites")}>
        <Ionicons name="heart" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("AppliedJobs")}
      >
        <Ionicons name="eye" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("InstructorDashboard")}
      >
        <FontAwesome5 name="dumbbell" size={rf(33)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("Notifications")}
      >
        <MaterialCommunityIcons name="bell" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.profileIcon}
        onPress={() => props.navigation.navigate("Profile")}
      >
        <FontAwesome name="user-circle" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#fff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
  profileIcon: {
    width: hp("4%"),
    height: hp("4%"),
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#fff",
  },
});
