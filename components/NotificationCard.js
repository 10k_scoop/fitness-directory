import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo, AntDesign } from "@expo/vector-icons";

export default function NotificationCard(props) {

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={styles.profile}>
        <Image
          style={{ width: "100%", height: "100%" }}
          source={require("../assets/profile.jpg")}
          resizeMode="cover"
        />
      </View>
      <View style={styles.Detail}>
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(12),
            fontWeight: "700",
            color: "#fff",
          }}
        >
          {props?.data?.data?.message}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("9%"),
    backgroundColor: "#D4A35A",
    borderRadius: 14,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
    paddingHorizontal: wp("3%"),
  },
  profile: {
    width: hp("6%"),
    height: hp("6%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Detail: {
    marginLeft: 10,
    width:'85%'
  },
});
