import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function MenuBtn({ txt, onPress }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={{ fontSize: rf(11), fontFamily: "MR" }}>{txt}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("4%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    marginLeft: 10,
    borderWidth: 1,
    borderColor: "#e5e5e5",
    paddingHorizontal: wp("7%"),
  },
  Icon: {
    width: "17%",
    height: "100%",
    alignItems: "flex-end",
  },
});
