import React from "react";
import { StyleSheet, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function TextInputField(props) {
  return (
    <View style={styles.LoginBody}>
      <TextInput
        style={styles.TextStyle}
        placeholder={props.placeholder}
        placeholderTextColor="#fff"
        keyboardType={props.type}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
        editable={props.email ? false : true}
        value={props?.value ? props?.value : null}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  LoginBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    justifyContent: "center",
    paddingHorizontal: "5%",
    marginTop: 10,
  },
  TextStyle: {
    fontWeight: "500",
    fontSize: rf(12),
    height: "90%",
    width: "100%",
    color: "#fff",
    fontFamily: "MR",
  },
});
