import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function Button(props) {
  return (
    <TouchableOpacity style={styles.BtnBody} onPress={props.onPress}>
      <Text style={styles.TextStyle}>{props.BtnText}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  BtnBody: {
    height: hp("5%"),
    width: wp("40%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    justifyContent: "center",
    paddingHorizontal: "5%",
    alignItems: "center",
    marginVertical: 10,
  },
  TextStyle: {
    fontFamily: "MR",
    fontSize: rf(12),
    fontWeight: "600",
    color: "#fff",
  },
});
