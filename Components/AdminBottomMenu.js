import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Feather, FontAwesome5, Entypo, Ionicons } from "@expo/vector-icons";

export default function AdminBottomMenu(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => props.navigation.navigate("AdminDashboard")}
      >
        <Entypo name="shop" size={rf(26)} color="#fff" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.profiletick}
        onPress={() => props.navigation.navigate("AdminApprovals")}
      >
        <Image
          source={require("../assets/profileImg.png")}
          style={{ width: "100%", height: "100%" }}
        />
      </TouchableOpacity>

      <TouchableOpacity>
        <Feather name="search" size={rf(32)} color="#fff" />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => props.navigation.navigate("AdminNotifications")}
      >
        <FontAwesome5 name="bell" size={rf(24)} color="#fff" />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => props.navigation.navigate("AdminSettings")}
      >
        <Ionicons name="settings" size={rf(26)} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#E2B269",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  Jobs: {
    width: "7%",
    height: "44%",
    backgroundColor: "#D4A35A",
    overflow: "hidden",
    borderRadius: 5,
  },
  profiletick: {
    width: "9%",
    height: "45%",
    overflow: "hidden",
    borderRadius: 5,
  },
  profile: {
    width: hp("4%"),
    height: hp("4%"),
    overflow: "hidden",
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
});
