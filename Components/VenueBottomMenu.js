import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {
  AntDesign,
  FontAwesome,
  FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
} from "@expo/vector-icons";

export default function VenueBottomMenu(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.Jobs}
        onPress={() => props.navigation.navigate("Myjobs")}
      >
        <MaterialIcons name="work" size={rf(23)} color="#D4A35A" />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.profiletick}
        onPress={() => props.navigation.navigate("applicants")}
      >
        <FontAwesome name="users" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.navigation.navigate("PostJob")}>
        <AntDesign name="pluscircleo" size={rf(32)} color="#E2B269" />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => props.navigation.navigate("VenueNotification")}
      >
        <MaterialCommunityIcons name="bell" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.profile}
        onPress={() => props.navigation.navigate("VenueProfile")}
      >
        <FontAwesome name="user-circle" size={rf(22)} color="#D4A35A" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#fff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  profiletick: {
    width: "9%",
    height: "45%",
    overflow: "hidden",
    borderRadius: 5,
  },
  profile: {
    width: hp("4%"),
    height: hp("4%"),
    overflow: "hidden",
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
  },
});
