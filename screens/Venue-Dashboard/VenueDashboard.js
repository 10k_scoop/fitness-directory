import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import GymCards from "../../components/GymCards";
import ApplicationCard from "./components/ApplicationCard";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import {
  logout,
  getUserData,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import {
  myJobs,
  myApplications,
} from "../../state-management/actions/Features/Actions";
import { connect } from "react-redux";
import * as firebase from "firebase";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const VenueDashboard = (props) => {
  const [loading, setLoading] = useState(false);
  const [myJobs, setMyJobs] = useState([]);
  const [applications, setApplications] = useState([]);
  const [userProfile, setUserProfile] = useState();
  const user = firebase.auth().currentUser;

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.getUserData(user.uid, setLoading);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.getUserData(user.uid, setLoading);
    props.myJobs(user.email, setLoading);
    props.myApplications(user.email, setLoading);
  }, []);


  useEffect(() => {
    setMyJobs(props?.my_jobs);
    setApplications(props?.my_applications);
    setUserProfile(props.get_user_details?.profile);
  }, [props]);

  return (

    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "700" }}>
              Active Jobs
            </Text>
          </View>
          {myJobs?.length < 1 && !loading && <Text>No related job found</Text>}
          {loading ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            myJobs?.map((item, index) => {
              return (
                <GymCards
                  onApplyClick={() =>
                    props.navigation.navigate("VenueJob", { data: item })
                  }
                  data={item}
                  key={index}
                  View
                  noHeart
                />
              );
            })
          )}

          <View style={styles.SecondTitle}>
            <Text style={{ fontSize: rf(20), fontWeight: "700" }}>
              Applications
            </Text>
          </View>
          {!loading && (
            <View style={{ marginBottom: hp("8%") }}>
              {props?.my_applications?.map((item, index) => {
                return (
                  <ApplicationCard
                    onViewApplication={() =>
                      props.navigation.navigate("SingleApplication", {
                        data: item,
                      })
                    }
                    data={item?.data}
                    key={index}
                  />
                );
              })}
            </View>
          )}
        </View>
      </ScrollView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "0%",
  },
  title: {
    width: wp("95%"),
    height: hp("5%"),
    justifyContent: "center",
    marginTop: hp("1%"),
  },
  SecondTitle: {
    width: wp("95%"),
    height: hp("8%"),
    alignItems: "center",
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
  my_jobs: state.main.my_jobs,
  my_applications: state.main.my_applications,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  logout,
  myJobs,
  getUserData,
  myApplications,
})(VenueDashboard);
