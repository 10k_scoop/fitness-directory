import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import {
  logout,
  getUserData,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import Skils from "../PostJob/components/Skills";
import { addSkills } from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";

const InstructorSkills = (props) => {
  const [skills, setSkills] = useState([]);
  const [skillText, setSkillText] = useState("");
  const [loading, setLoading] = useState(false);
  const user = firebase.auth().currentUser;
  const onAddSkill = () => {
    if (skillText != "") {
      setSkills((prevstate) => [...prevstate, skillText]);
      setSkillText("");
    }
  };
  const onRemoveSkill = (value) => {
    let filteredArray = skills.filter((item) => item !== value);
    setSkills(filteredArray);
  };

  useEffect(() => {
    setLoading(true);
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    setSkills(
      props.get_user_details?.skills ? props.get_user_details?.skills : []
    );
  }, [props]);

  const onSubmit = () => {
    setLoading(true);
    props.addSkills(skills, user.uid, setLoading);
  };

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />

      <View style={styles.cont1}>
        <View style={styles.searchBar}>
          <TextInput
            style={{
              width: "100%",
              height: "90%",
              paddingHorizontal: 10,
              fontFamily: "MR",
            }}
            placeholder="Add new Skill"
            color="#222"
            placeholderTextColor="#222"
            value={skillText}
            onChangeText={(val) => setSkillText(val)}
          />
        </View>
        <TouchableOpacity style={styles.addBtn} onPress={onAddSkill}>
          <Text
            style={{
              fontFamily: "MS",
              fontSize: rf(13),
              fontWeight: "500",
              color: "#fff",
            }}
          >
            Add
          </Text>
        </TouchableOpacity>
      </View>
      {!loading && (
        <View style={styles.SkilCategory}>
          {skills.map((item, index) => {
            return (
              <Skils
                Skill={item}
                key={index}
                onRemove={() => onRemoveSkill(item)}
              />
            );
          })}
        </View>
      )}
      <View
        style={{
          alignItems: "center",
          flex: 1,
          justifyContent: "flex-end",
          marginBottom: hp("15%"),
        }}
      >
        <TouchableOpacity
          style={styles.submitBtn}
          onPress={() => !loading && onSubmit()}
        >
          {!loading ? (
            <Text style={{ fontFamily: "MS", fontSize: rf(14), color: "#fff" }}>
              Submit
            </Text>
          ) : (
            <ActivityIndicator size="small" color="#fff" />
          )}
        </TouchableOpacity>
      </View>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  addBtn: {
    height: hp("5%"),
    width: wp("25%"),
    backgroundColor: "#D4A35A",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  cont1: {
    alignItems: "center",
    marginTop: hp("3%"),
  },
  searchBar: {
    height: hp("5%"),
    width: wp("90%"),
    backgroundColor: "transparent",
    borderRadius: 100,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#D4A35A",
  },
  SkilCategory: {
    flexWrap: "wrap",
    flexDirection: "row",
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: wp("2%"),
    marginTop: hp("5%"),
  },
  submitBtn: {
    height: hp("5%"),
    width: wp("85%"),
    backgroundColor: "#D4A35A",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, addSkills, getUserData })(
  InstructorSkills
);
