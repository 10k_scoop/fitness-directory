import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  TextInput,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import AdminBottomMenu from "../../components/AdminBottomMenu";
import { connect } from "react-redux";
import { getAllPayments } from "../../state-management/actions/Features/Actions";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { AntDesign } from "@expo/vector-icons";
import PaymentCard from "./components/PaymentCard";

const PaymentRecord = (props) => {
  const [searchText, setSearchText] = useState("");
  const [loading, setLoading] = useState(false);
  const [payments, setPayments] = useState([]);
  const [fullData, setFullData] = useState([]);

  useEffect(() => {
      setLoading(true)
      props.getAllPayments(setLoading)
  }, []);

  useEffect(() => {
    setPayments(props?.get_all_payments);
    setFullData(props?.get_all_payments);
  }, [props]);

  const filterItems = (query) => {
    if (query == "") {
      setPayments(props.get_all_payments);
    } else {
      let data = payments?.data?.data.filter(
        (el) =>
          el.recruiterEmail.toLowerCase().indexOf(query.toLowerCase()) !== -1
      );
      setPayments(data);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    )
  }
  

  return (
    <View style={styles.container}>
      <Header onLogoutPress={()=>props.logout()} onHomeClick={()=>props.navigation.navigate("AdminDashboard")} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.SearchWrapper}>
            <View style={styles.Search}>
              <TextInput
                style={styles.TextField}
                placeholder="Search here by venue email"
                placeholderTextColor="#222"
                onChangeText={(val) => {
                  setSearchText(val);
                  filterItems(val);
                }}
              />
            </View>
            <TouchableOpacity
              style={styles.searchIcon}
              onPress={() => onSearch(searchText)}
            >
              <AntDesign name="search1" size={rf(18)} color="white" />
            </TouchableOpacity>
          </View>

          {loading && (
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <ActivityIndicator size="large" color="orange" />
            </View>
          )}

          {payments?.map((item, index) => {
            return (
              <PaymentCard
                title={""}
                key={index}
                data={item?.data?.data}
                paymentData={item?.data?.paymentIntent}
                onApplyClick={() =>
                  props.navigation.navigate("ViewSinglePayment", { data: item })
                }
              />
            );
          })}
        </View>
      </ScrollView>
      <AdminBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("10%"),
  },
  SearchWrapper: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  Search: {
    width: wp("80%"),
    height: hp("5%"),
    borderWidth: 0.3,
    borderColor: "#e5e5e5",
    borderRadius: 100,
    borderColor: "#aca7a6",
    paddingHorizontal: "2%",
    fontSize: rf(11),
    fontFamily: "MR",
  },
  TextField: {
    width: "80%",
    height: "100%",
    paddingHorizontal: wp("3%"),
  },
  searchIcon: {
    alignItems: "center",
    justifyContent: "center",
    left: 10,
    backgroundColor: "orange",
    width: wp("8%"),
    height: wp("8%"),
    borderRadius: 100,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
  get_all_payments: state.main.get_all_payments,
});
export default connect(mapStateToProps, {logout, getAllPayments })(PaymentRecord);
