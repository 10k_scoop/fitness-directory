import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import AdminBottomMenu from "../../components/AdminBottomMenu";

export default function AdminSettings({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onHomeClick={()=>props.navigation.navigate("AdminDashboard")} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <TouchableOpacity style={styles.Cards}>
            <Text style={styles.CardTxt}>Change Password</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Cards}>
            <Text style={styles.CardTxt}>View reports</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.Cards}>
            <Text style={styles.CardTxt}>Change Pricing</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.Cards}
            onPress={() => navigation.navigate("PaymentRecord")}
          >
            <Text style={styles.CardTxt}>Payments Record</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.ApprovalCard}
            onPress={() => navigation.navigate("AdminApprovals")}
          >
            <Text style={styles.CardTxt}>Approvals</Text>
            <View style={styles.circle}>
              <Text style={styles.circletxt}>5</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <AdminBottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },

  Main: {
    width: wp("100%"),
    height: hp("100%"),
  },
  Cards: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#E2B269",
    marginTop: hp("2%"),
    justifyContent: "center",
    paddingHorizontal: wp("4%"),
  },
  CardTxt: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#fff",
  },
  ApprovalCard: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#E2B269",
    marginTop: hp("2%"),
    alignItems: "center",
    paddingHorizontal: wp("4%"),
    flexDirection: "row",
    justifyContent: "space-between",
  },
  circle: {
    width: hp("4%"),
    height: hp("4%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  circletxt: {
    fontSize: rf(15),
    fontWeight: "700",
  },
});
