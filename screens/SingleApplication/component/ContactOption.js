import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome, MaterialIcons, Entypo } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function ContactOption(props) {
  return (
    <View style={styles.ContactOptionWrapper}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.CallIcon} onPress={props.onCallPress}>
          <FontAwesome name="phone" size={25} color="#fff" />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: "MM",
            fontSize: rf(10),
            marginTop: 5,
            fontWeight: "400",
          }}
        >
          Call Phone
        </Text>
      </View>
      <View style={styles.container}>
        <TouchableOpacity style={styles.CallIcon} onPress={props.onMailPress}>
          <Entypo name="mail" size={25} color="#fff" />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: "MM",
            fontSize: rf(10),
            marginTop: 5,
            fontWeight: "400",
          }}
        >
          Send Mail
        </Text>
      </View>
      <View style={styles.container}>
        <TouchableOpacity style={styles.CallIcon} onPress={props.onChatPress}>
          <MaterialIcons name="textsms" size={25} color="#fff" />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: "MM",
            fontSize: rf(10),
            marginTop: 5,
            fontWeight: "400",
          }}
        >
          Chat
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ContactOptionWrapper: {
    height: hp("13%"),
    paddingHorizontal: wp("15%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  CallIcon: {
    height: hp("6%"),
    width: hp("6%"),
    backgroundColor: "#E2B269",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
  },
});
