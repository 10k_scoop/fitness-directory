import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function ContactBtn(props) {
  return (
    <View style={styles.BtnContainer}>
      <TouchableOpacity style={styles.HireBtn} onPress={props.onPress}>
        <Text
          style={{
            fontFamily: "MB",
            fontSize: 15,
            fontWeight: "400",
            color: "#fff",
          }}
        >
          
          {props.text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  BtnContainer: {
    height: hp("14%"),
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: wp("10%"),
    alignItems: "center",
  },
  HireBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
