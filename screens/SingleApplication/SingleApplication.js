import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Platform,
  Linking,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import ContactOption from "./component/ContactOption";
import ResumeUpload from "./component/ResumeUpload";
import ContactBtn from "./component/ContactBtn";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import { getUserData } from "../../state-management/actions/auth/FirebaseAuthActions";
import { viewJob } from "../../state-management/actions/Features/Actions";
import { Picker } from "@react-native-picker/picker";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { FontAwesome5 } from "@expo/vector-icons";

const SingleApplication = (props) => {
  const [loading, setLoading] = useState(true);
  const [showResume, setShowResume] = useState(false);
  const [hours, setHours] = useState(1);
  const [user, setUser] = useState();
  const [job, setJob] = useState();
  const [showPaymentPopup, setShowPaymentPopup] = useState(false);
  var data = props.route.params.data?.data;
  const user2 = firebase.auth().currentUser;

  var perDayHour = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24,
  ];
  var perDay = [1, 2, 3, 4, 5, 6];

  useEffect(() => {
    setLoading(true);
    props.viewJob(data?.jobId, setLoading);
  }, []);

  useEffect(() => {
    setLoading(true);
    props.getUserData(data.applicant, setLoading);
  }, []);

  useEffect(() => {
    setUser(props.get_user_details);
  }, [props]);

  useEffect(() => {
    setJob(props.view_job);
  }, [props]);

  const onContinuePayment = () => {
    if (hours) {
      let pdata = {
        applicationId: props.route.params.data?.id,
        jobId: data?.jobId,
        applicantId: data?.applicant,
        applicantDetails: props.get_user_details,
        hours: hours,
        price: parseInt(job?.price),
        total:job?.salary == "Fixed hourly £15"? parseInt(job?.price) * hours * 6: parseInt(job?.price) * hours,
        recruiterEmail: user2.email,
        recruiterId: user2.uid,
        jobDetails: job,
      };
      props.navigation.navigate("PaymentMethod", { data: pdata });
    } else {
      alert("Select Hours");
    }
  };

  const onCallClick = () => {
    let phoneNumber = user.contact;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${user.contact}`;
    } else {
      phoneNumber = `tel:${user.contact}`;
    }
    Linking.openURL(phoneNumber);
  };

  if (loading) {
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator size="large" color="orange" />
    </View>;
  }

  return (
    <View style={styles.container}>
      {/* Resume Viewer */}
      {showResume && (
        <View style={styles.resumeViewWrapper}>
          <TouchableOpacity
            style={styles.resumeCloseBtn}
            onPress={() => setShowResume(!showResume)}
          >
            <FontAwesome5 name="times-circle" size={24} color="white" />
          </TouchableOpacity>
          <Image
            style={{ height: "100%", width: "100%" }}
            source={{ uri: data.applicantDocs }}
            resizeMode="contain"
          />
        </View>
      )}

      {/* Payment Popup */}
      {showPaymentPopup && (
        <View style={styles.paymentPopup}>
          <Text style={[styles.paymentPopupItemTitle, { color: "red" }]}>
            NOTE:- minimum payment can be done for 6 days
          </Text>
          <Text style={styles.paymentPopupItemTitle}>
            Salary:- {job?.salary}
          </Text>
          <View style={[styles.paymentPopupPicker, { overflow: "hidden" }]}>
            {job?.salary == "Fixed hourly £15" ? (
              <Picker
                selectedValue={hours}
                onValueChange={(itemValue, itemIndex) => setHours(itemValue)}
                itemStyle={{ color: "#222", fontSize: rf(18), zIndex: 999999 }}
                style={{
                  width: "100%",
                  color: "black",
                }}
              >
                <Picker.Item label="Select hours" value={null} />

                {perDayHour.map((item, index) => {
                  return (
                    <Picker.Item
                      label={item.toString()}
                      value={item}
                      key={index}
                    />
                  );
                })}
              </Picker>
            ) : (
              <Picker
                selectedValue={hours}
                onValueChange={(itemValue, itemIndex) => setHours(itemValue)}
                itemStyle={{ color: "#222", fontSize: rf(18), zIndex: 999999 }}
                style={{
                  width: "100%",
                  color: "black",
                }}
              >
                <Picker.Item label="Select days" value={null} />

                {perDay.map((item, index) => {
                  return (
                    <Picker.Item
                      label={item.toString()}
                      value={item}
                      key={index}
                    />
                  );
                })}
              </Picker>
            )}
          </View>
          <Text style={styles.paymentPopupItemTitle}>
            Total amount to be paid:-{" "}
            {job?.salary == "Fixed hourly £15"
              ? hours
                ? "£" + parseInt(job?.price) * hours * 6
                : "Select hours"
              : hours
              ? "£" + parseInt(job?.price) * hours
              : "Select days"}
          </Text>
          <View style={styles.continuePaymentBtnWrapper}>
            <TouchableOpacity
              style={[
                styles.continuePaymentBtn,
                { backgroundColor: "#cdcdcd" },
              ]}
              onPress={() => setShowPaymentPopup(!showPaymentPopup)}
            >
              <Text style={styles.continuePaymentBtnText}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.continuePaymentBtn}
              onPress={onContinuePayment}
            >
              <Text style={styles.continuePaymentBtnText}>
                Continue to payment
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
      {/* Payment Popup */}
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />

      <ScrollView>
        <View style={styles.ProfileContainer}>
          <View style={styles.BottomWidth}>
            <View style={styles.ProfilePicBody}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={{ uri: data.applicantProfile }}
                resizeMode="cover"
              />
            </View>
            <View style={styles.ProfileNAmeBody}>
              <Text style={styles.Text}>{data.title}</Text>
              <Text style={styles.SecondaryText}>{data?.describtion}</Text>
            </View>
          </View>
        </View>
        <View style={styles.JohnTxtWrapper}>
          <Text style={styles.Text}>{user?.name}</Text>
          <Text style={styles.SecondaryText}>{user?.address}</Text>
        </View>
        <ContactOption
          onCallPress={() => onCallClick()}
          onMailPress={() =>
            Linking.openURL(
              `mailto:${user.email}?subject=SendMail&body=Description`
            )
          }
        />
        <View style={styles.ResumeTxt}>
          <Text
            style={{ fontFamily: "MB", fontSize: rf(13), fontWeight: "700" }}
          >
            Resume Attachment
          </Text>
        </View>
        <View style={styles.ResumeContainer}>
          <ResumeUpload
            image={data.applicantDocs}
            onPress={() => setShowResume(!showResume)}
          />
        </View>
        <View style={styles.ExperienceTxtWrapper}>
          <Text
            style={{ fontFamily: "MS", fontSize: rf(13), fontWeight: "700" }}
          >
            Work Experience
          </Text>
          {user?.skills?.map((item, index) => {
            return (
              <Text
                key={index}
                style={{
                  fontFamily: "MR",
                  fontSize: rf(11),
                  fontWeight: "400",
                  marginVertical: 4,
                }}
              >
                {item}
              </Text>
            );
          })}
        </View>
        <ContactBtn
          text={
            data?.hiredByEmail.length == 0
              ? "Hire Now"
              : data?.hiredByEmail == data?.author
              ? "Hired By You"
              : "Hired By Someone"
          }
          onPress={() => {
            data?.hiredByEmail.length == 0
              ? setShowPaymentPopup(true)
              : data?.hiredByEmail == data?.author
              ? alert("Already hired by you")
              : alert("Hired by someone else");
          }}
        />
      </ScrollView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ProfileContainer: {
    height: hp("12%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
  },
  ProfilePicBody: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  ProfileNAmeBody: {
    height: "100%",
    paddingHorizontal: "5%",
    justifyContent: "center",
  },
  BottomWidth: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#A59F9F",
    alignItems: "center",
  },
  JohnTxtWrapper: {
    height: hp("7%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  ResumeTxt: {
    height: hp("4%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    marginBottom: 5,
  },
  Text: {
    fontSize: rf(12),
    fontWeight: "500",
    color: "#222",
    fontFamily: "MB",
  },
  SecondaryText: {
    fontSize: rf(11),
    fontWeight: "400",
    color: "#A59F9F",
    fontFamily: "MM",
  },
  ResumeContainer: {
    height: hp("18%"),
    paddingHorizontal: wp("5%"),
  },
  ExperienceTxtWrapper: {
    height: hp("10%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  paymentPopup: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("30%"),
    backgroundColor: "white",
    position: "absolute",
    zIndex: 9999,
    top: hp("40%"),
    left: wp("5%"),
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    padding: 10,
    elevation: 5,
    alignItems: "center",
  },
  paymentPopupPicker: {
    width: "100%",
    height: hp("5%"),
    borderWidth: 1,
    borderColor: "#e5e5e5",
    borderRadius: 5,
    justifyContent: "center",
    marginBottom: 10,
  },
  paymentPopupItemTitle: {
    textAlign: "left",
    width: "100%",
    marginBottom: 10,
    fontWeight: "bold",
    color: "black",
  },
  continuePaymentBtn: {
    width: "45%",
    height: hp("5%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    color: "white",
  },
  continuePaymentBtnText: {
    color: "white",
    fontSize: rf(12),
    fontWeight: "bold",
  },
  continuePaymentBtnWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  resumeViewWrapper: {
    width: wp("90%"),
    height: hp("70%"),
    position: "absolute",
    backgroundColor: "#E2B269",
    zIndex: 9999999,
    left: wp("5%"),
    top: hp("15%"),
    borderRadius: 10,
  },
  resumeCloseBtn: {
    zIndex: 99999999999999,
    position: "absolute",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  view_job: state.main.view_job,
});
export default connect(mapStateToProps, { logout, getUserData, viewJob })(
  SingleApplication
);
