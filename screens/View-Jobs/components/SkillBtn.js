import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo, AntDesign } from "@expo/vector-icons";

export default function SkillBtn({ txt }) {
  return (
    <View style={styles.container}>
      <Text
        style={{
          fontFamily: "MS",
          fontSize: rf(12),
          fontWeight: "700",
          color: "#fff",
        }}
      >
        {txt}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("30%"),
    height: hp("5%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    margin: 5,
  },
});
