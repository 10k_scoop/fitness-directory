import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo, AntDesign } from "@expo/vector-icons";

export default function JobDetailCards({ txt, txt1 }) {
  return (
    <View style={styles.container}>
      <Text style={{ fontFamily: "MS", fontSize: rf(14), fontWeight: "700" }}>
        {txt}
      </Text>
      <Text style={{ fontFamily: "MR", fontSize: rf(13) }}>{txt1}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("3.5%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
