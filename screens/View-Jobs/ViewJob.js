import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import Header1 from "./components/Header1";
import SkillBtn from "./components/SkillBtn";
import JobDetailCards from "./components/JobDetailCards";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import { applyToJob } from "../../state-management/actions/Features/Actions";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import * as firebase from "firebase";

const ViewJob = (props) => {
  var data = props?.route?.params?.data;
  var date = data?.date.toDate().toDateString();
  const user = firebase.auth().currentUser;
  const onApply = () => {
    props.navigation.navigate("ApplyToJob", { data: data });
  };

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <Header1
            title={data?.title}
            location={data?.location}
            onBackClick={() => props.navigation.goBack()}
          />
          <View style={styles.DiscriptionHeading}>
            <Text style={styles.Font1}>Job Discription</Text>
            <View style={styles.BottomBar}></View>
          </View>
          <View style={styles.Discription}>
            <Text style={{ fontSize: rf(15), letterSpacing: 1 }}>
              {data?.describtion}
            </Text>
          </View>
          <View style={styles.SkillsHeading}>
            <Text style={{ fontSize: rf(17), fontFamily: "MM" }}>Skills</Text>
          </View>
          <View style={styles.BtnRow}>
            {data?.skills.map((item, index) => (
              <SkillBtn txt={item} key={index} />
            ))}
          </View>
          <View style={styles.JobDetails}>
            <Text
              style={{
                fontFamily: "MR",
                fontSize: rf(15),
                marginBottom: 10,
                marginTop: 5,
              }}
            >
              Jobs Details
            </Text>
            <JobDetailCards txt="Total Position:" txt1={data?.totalPositions} />
            <JobDetailCards txt="Job Type:" txt1={data?.jobType} />
            <JobDetailCards txt="Job Shift:" txt1={data?.jobShift} />
            <JobDetailCards txt="Gender:" txt1={data?.gender} />
            <JobDetailCards
              txt="Experience:"
              txt1={data?.experience + " Years"}
            />
            <JobDetailCards txt="Posting Date:" txt1={date} />
            <JobDetailCards
              txt="Salary:"
              txt1={
                data?.salary == "Fixed classes £32"
                  ? "Fixed classes £ 26.25 pc"
                  : "Fixed hourly £ 12.75 ph"
              }
            />
          </View>
        </View>
      </ScrollView>

      <TouchableOpacity
        style={styles.ApplyBtn}
        onPress={() =>
          data.applicants.includes(user.email)
            ? alert("Already applied")
            : onApply()
        }
      >
        <Text
          style={{
            fontFamily: "MR",
            fontSize: rf(16),
            fontWeight: "700",
            color: "#fff",
          }}
        >
          {data.applicants.includes(user.email)
            ? "Already applied"
            : "Apply Now"}
        </Text>
      </TouchableOpacity>

      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "25%",
  },
  SearchBar: {
    width: wp("95%"),
    height: hp("7%"),
    borderWidth: 1,
    borderColor: "#e5e5e5",
    borderRadius: 100,
    marginTop: "6%",
    borderColor: "#aca7a6",
    paddingHorizontal: "5%",
    fontSize: rf(16),
  },
  DiscriptionHeading: {
    width: wp("95%"),
    height: hp("5%"),
    alignItems: "center",
  },
  BottomBar: {
    width: "22%",
    height: "4%",
    backgroundColor: "#D4A35A",
    marginTop: 3,
  },
  Font1: {
    fontSize: rf(18),
    fontWeight: "700",
    color: "#D4A35A",
    fontFamily: "MB",
  },
  Discription: {
    width: wp("95%"),
    paddingHorizontal: 10,
    justifyContent: "flex-start",
    marginBottom: 10,
  },
  SkillsHeading: {
    width: wp("95%"),
    height: hp("5%"),
    justifyContent: "center",
    paddingHorizontal: "3%",
  },
  BtnRow: {
    width: wp("95%"),
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: wp("3%"),
    flexWrap: "wrap",
  },
  JobDetails: {
    width: wp("95%"),
    height: hp("30%"),
    paddingHorizontal: "3%",
    marginBottom: "10%",
    justifyContent: "center",
  },
  ApplyBtn: {
    width: wp("90%"),
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    position: "absolute",
    bottom: hp("9%"),
    justifyContent: "center",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  apply_to_job: state.main.apply_to_job,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { applyToJob, logout })(ViewJob);
