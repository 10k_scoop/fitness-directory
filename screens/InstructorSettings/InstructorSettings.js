import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Switch, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";

const InstructorSettings = (props) => {
  const [notifications, setNotifications] = useState(false);
  const toggleSwitch = () =>
    setNotifications((previousState) => !previousState);

  useEffect(() => {
    console.log(props.get_user_details);
  }, [props]);

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />

      <View
        style={[styles.notifications, { marginTop: hp("5%") }]}
        onPress={() => props.navigation.navigate("InstructorSettings")}
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          Notifications
        </Text>
        <Switch
          trackColor={{ false: "#767577", true: "white" }}
          thumbColor={notifications ? "orange" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={notifications}
        />
      </View>
      <TouchableOpacity
        style={styles.SetingBody}
        onPress={() => props.navigation.navigate("InstructorSkills")}
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          My Skills
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.SetingBody}
        onPress={() => props.navigation.navigate("ChangePassword")}
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          Change Password
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.SetingBody}
        onPress={() => props.navigation.navigate("InstructorBankDetails")}
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          Bank Details
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.SetingBody}
        onPress={() => props.navigation.navigate("InstructorSettings")}
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          Contact Us
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.SetingBody}
        onPress={() =>
          props.navigation.navigate("InstructorSettings", {
            role: "instructor",
          })
        }
      >
        <Text
          style={{
            fontFamily: "MS",
            fontSize: rf(13),
            fontWeight: "500",
            color: "#fff",
          }}
        >
          About Us
        </Text>
      </TouchableOpacity>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  SetingBody: {
    height: hp("6%"),
    width: wp("95%"),
    backgroundColor: "#D4A35A",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  notifications: {
    height: hp("6%"),
    width: wp("95%"),
    backgroundColor: "#D4A35A",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout })(InstructorSettings);
