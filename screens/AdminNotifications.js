import React, { useEffect,useState } from "react";
import { StyleSheet, Text,ActivityIndicator, View, ScrollView } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import NotificationCard from "../components/NotificationCard";
import { connect } from "react-redux";
import { logout } from "../state-management/actions/auth/FirebaseAuthActions";
import AdminBottomMenu from "../components/AdminBottomMenu";
import { getAllNotifications } from "../state-management/actions/Features/Actions";
import * as firebase from "firebase";

const AdminNotifications = (props) => {

  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;
  useEffect(() => {
    props.getAllNotifications(setLoading);
  }, []);

  useEffect(() => {
    setNotifications(props.get_all_notifications);
  }, [props]);


  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.goBack()}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text
              style={{ fontFamily: "MB", fontSize: rf(22), fontWeight: "700" }}
            >
              Notifications
            </Text>
          </View>
          {
            loading &&
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              <ActivityIndicator size="large" color="orange" />
            </View>
          }
          {notifications?.map((item, index) => {
            console.log(item)
            if(item?.data?.to==user.email){
              return <NotificationCard key={index} data={item} />;
            }
          })}
        </View>
      </ScrollView>
      <AdminBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "15%",
  },
  title: {
    width: wp("95%"),
    height: hp("8%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_all_notifications: state.main.get_all_notifications,
});
export default connect(mapStateToProps, { logout,getAllNotifications })(AdminNotifications);
