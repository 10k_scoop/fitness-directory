import React, { useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import TextInputField from "../../components/TextInputField";
import Button from "../../components/Button";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { forgotPassword } from "../../state-management/actions/auth/FirebaseAuthActions";

const ForgotPassword = (props) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);

  const onSubmit = () => {
    if (email != "") {
      props.forgotPassword(email, setLoading);
      setEmail("");
    } else {
      alert("Enter your email");
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <View style={styles.LogoWrapper}>
          <View style={styles.LogoBody}>
            <Image
              style={{ height: "80%", width: "80%" }}
              source={require("../../assets/logo.png")}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.MailWrapper}>
          <TextInputField
            placeholder="Enter Your Email"
            onChangeText={(val) => setEmail(val)}
            type="email-address"
            style={{ fontFamily: "MR" }}
          />
        </View>
        <View style={styles.BtnWrapper}>
          <Button
            BtnText={
              loading ? (
                <ActivityIndicator size="small" color="#fff" />
              ) : (
                "Send reset email"
              )
            }
            onPress={() => {
              loading ? undefined : setLoading(true);
              onSubmit();
            }}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  PicWrapper: {
    height: hp("100%"),
    width: wp("100%"),
  },
  Layer: {
    height: hp("100%"),
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    justifyContent: "flex-end",
  },
  LogoBody: {
    height: "50%",
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  MailWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  BtnWrapper: {
    height: hp("25%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
});
export default connect(mapStateToProps, { forgotPassword })(ForgotPassword);
