import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import Header1 from "./components/Header1";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { applyToJob } from "../../state-management/actions/Features/Actions";
import * as ImagePicker from "expo-image-picker";
import * as firebase from "firebase";

const ApplyToJob = (props) => {
  const [details, setDetails] = useState();
  const [cv, setCv] = useState();
  const [loading, setLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const user = firebase.auth().currentUser;
  let data = props.route.params.data;
  console.log(data)
  let applicants = data.applicants;
  useEffect(() => {
    if (props.get_user_details) {
      setDetails(props.get_user_details);
    }
  }, [props]);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          //alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const uploadCv = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [5, 7],
      quality: 1,
    });

    if (!result.cancelled) {
      setCv(result.uri);
    }
  };

  const onRemoveCv = () => {
    setCv(null);
  };

  const onContinue = () => {
    setLoading(true);

    applicants.push(user.email);
    let details = {
      applicantEmail:user.email,
      applicant: user.uid,
      applicantName: props.get_user_details.name,
      hiredByEmail:props.get_user_details?.hiredByEmail,
      applicantProfile:
        "https://mobirise.com/bootstrap-template/profile-template/assets/images/timothy-paul-smith-256424-1200x800.jpg",
      author: data.author,
      jobId: data.id,
      status: "Applied",
      title: data.title,
      applicants: data.applicants,
      applicantDocs: cv,
    };
    props.applyToJob(details, setLoading, setSubmitted);
  };

  if (submitted) {
    props.navigation.navigate("AppliedSuccesfull");
  }

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <Header1 onbackClick={() => props.navigation.goBack()} data={data}/>
          <View style={styles.InformationBox}>
            <View style={styles.FirstRow}>
              <Text style={{ fontSize: rf(16) }}>
                Is your information up to date?
              </Text>
            </View>
            <View style={styles.SecondRow}>
              <Text style={{ fontSize: rf(17) }}>{details?.name}</Text>
            </View>
            <View style={styles.ThirdRow}>
              <Text style={{ fontSize: rf(13), marginVertical: hp("1%") }}>
                Email: {details?.email}
              </Text>
              <Text style={{ fontSize: rf(13) }}>
                Address: {details?.address}
              </Text>
            </View>
            {details?.skills?.map((item, index) => {
              return (
                <View style={styles.LastRow} key={index}>
                  <Text style={{ fontSize: rf(13) }}>Skill: {item}</Text>
                </View>
              );
            })}

            <View style={styles.LastRow}>
              <Text style={{ fontSize: rf(13) }}>
                Contact: {details?.contact}
              </Text>
            </View>
            <View style={styles.LastRow}>
              <Text style={{ fontSize: rf(13) }}>
                CV | Documents: {!details?.documents && cv == null && "none"}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  marginVertical: 15,
                }}
              >
                {cv && (
                  <View style={styles.cvView}>
                    <TouchableOpacity
                      style={styles.removeCv}
                      onPress={() => onRemoveCv()}
                    >
                      <Text style={{ color: "#fff" }}>X</Text>
                    </TouchableOpacity>
                    <Image
                      source={{ uri: cv }}
                      style={{ width: "100%", height: "100%" }}
                    />
                  </View>
                )}
              </View>
              <TouchableOpacity style={styles.uploadCv} onPress={uploadCv}>
                <Text style={[styles.Font1, { fontSize: rf(12) }]}>
                  Upload CV
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.BtnRow}>
              <TouchableOpacity
                style={styles.EditBtn}
                onPress={() => props.navigation.navigate("Profile")}
              >
                <Text style={styles.Font1}>Edit Profile</Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            style={styles.ContinueBtn}
            onPress={() => (loading ? undefined : onContinue())}
          >
            {!loading ? (
              <Text style={styles.Font2}>Continue</Text>
            ) : (
              <ActivityIndicator size="small" color="#fff" />
            )}
          </TouchableOpacity>
          <TouchableOpacity style={styles.BackBtn}>
            <Text style={styles.Font3}>Back</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  InformationBox: {
    width: wp("90%"),
    backgroundColor: "#F7F7F7",
    borderRadius: 10,
    alignItems: "center",
    marginBottom: "14%",
  },
  FirstRow: {
    width: "95%",
    height: hp("5%"),
    justifyContent: "center",
    marginVertical: hp("1%"),
  },
  SecondRow: {
    width: "95%",
    marginVertical: hp("1%"),
    alignItems: "center",
    justifyContent: "center",
  },
  ThirdRow: {
    width: "95%",
    justifyContent: "space-evenly",
    marginVertical: hp("1%"),
  },
  LastRow: {
    width: "95%",
    justifyContent: "center",
    marginVertical: hp("1%"),
  },
  BtnRow: {
    width: "95%",
    height: hp("8%"),
    justifyContent: "flex-end",
    alignItems: "center",
    marginVertical: hp("1%"),
  },
  EditBtn: {
    width: "35%",
    height: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
  },
  ContinueBtn: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "#E2B269",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "7%",
  },
  BackBtn: {
    width: wp("90%"),
    height: hp("6%"),
    borderWidth: 1,
    borderColor: "#E2B269",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
  Font2: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#fff",
  },
  Font3: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#E2B269",
  },
  uploadCv: {
    width: "25%",
    height: hp("4%"),
    borderRadius: 10,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  cvView: {
    width: hp("5%"),
    height: hp("5%"),
    borderRadius: 5,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: "grey",
  },
  removeCv: {
    width: 20,
    height: 20,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    position: "absolute",
    right: -10,
    zIndex: 999999,
    top: -10,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  apply_to_job: state.main.apply_to_job,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, applyToJob })(ApplyToJob);
