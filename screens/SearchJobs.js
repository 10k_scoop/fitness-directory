import React from "react";
import { StyleSheet, Text, View, ScrollView, TextInput } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import MenuBtn from "../components/MenuBtn";
import GymCards from "../components/GymCards";
import BottomMenu from "../components/BottomMenu";

export default function SearchJobs({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onHomeClick={() => navigation.navigate("InstructorDashboard")} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <TextInput placeholder="Search here..." style={styles.SearchBar} />
          <View style={styles.Menu}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <MenuBtn txt="Yoga" />
              <MenuBtn txt="Personal Trainer" />
              <MenuBtn txt="Zumbakara" />
              <MenuBtn txt="Yoga" />
              <MenuBtn txt="Yoga" />
              <MenuBtn txt="Yoga" />
            </ScrollView>
          </View>

          <GymCards />
          <GymCards />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  SearchBar: {
    width: wp("95%"),
    height: hp("5%"),
    borderWidth: 0.3,
    borderColor: "#e5e5e5",
    borderRadius: 100,
    marginTop: "6%",
    borderColor: "#aca7a6",
    paddingHorizontal: "5%",
    fontSize: rf(11),
  },
});
