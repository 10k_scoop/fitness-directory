import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Switch,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import {
  logout,
  getUserData,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import { FontAwesome } from "@expo/vector-icons";
import TextInputField from "../../components/TextInputField";
import Button from "../../components/Button";
import { updateBank } from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";
import VenueBottomMenu from "../../components/VenueBottomMenu";

const VenueBankDetails = (props) => {
  const [bankName, setBankName] = useState("");
  const [accountName, setAccountName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");
  const [loading, setLoading] = useState(false);
  const [contentLoading, setContentLoading] = useState(true);

  const user = firebase.auth().currentUser;

  useEffect(() => {
    setContentLoading(true);
    props.getUserData(user.uid, setContentLoading);
  }, []);

  useEffect(() => {
    if (props?.get_user_details) {
      setBankName(props?.get_user_details?.bankName);
      setAccountName(props?.get_user_details?.accountName);
      setAccountNumber(props?.get_user_details?.accountNumber);
    }
  }, [props]);

  const onUpdateBank = () => {
    try {
      setLoading(true);
      let data = {
        bankName: bankName,
        accountName: accountName,
        accountNumber: accountNumber,
      };
      props?.updateBank(data, user?.uid, setLoading);
    } catch (e) {
      alert(e.message);
    }
  };

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />
      <View style={styles.body}>
        <View style={styles.bodyHeader}>
          <FontAwesome name="bank" size={rf(30)} color="black" />
          <Text style={styles.headerText}>Edit Bank details</Text>
        </View>
        <Text style={styles.notetText}>
          Note : {" "}
          <Text>
             Change of bank details need to be emailed to:
            director@tfdirectory.co.uk
          </Text>
        </Text>
        {contentLoading ? (
          <ActivityIndicator size="large" color="#222" />
        ) : (
          <View style={styles.formBody}>
            <TextInputField
              placeholder="Bank Name"
              onChangeText={(val) => setBankName(val)}
              value={bankName}
            />
            <TextInputField
              placeholder="Account Name"
              onChangeText={(val) => setAccountName(val)}
              value={accountName}
            />
            <TextInputField
              placeholder="Account Number or IBAN"
              type="number-pad"
              onChangeText={(val) => setAccountNumber(val)}
              value={accountNumber}
            />
            <Button
              BtnText={
                loading ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  "Update"
                )
              }
              onPress={() => (loading ? null : onUpdateBank())}
            />
          </View>
        )}
      </View>

      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  body: {
    flex: 1,
    alignItems: "center",
    marginVertical: hp("3%"),
  },
  bodyHeader: {
    marginBottom: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    fontSize: rf(20),
    fontWeight: "700",
    marginVertical: wp("2%"),
  },
  formBody: {
    alignItems: "center",
    justifyContent: "center",
  },
  notetText:{
    fontSize: rf(13),
    fontWeight: "700",
    marginVertical: wp("2%"),
    textAlign:'center',
    color:'red'
  }
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, updateBank, getUserData })(
  VenueBankDetails
);
