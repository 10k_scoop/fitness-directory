import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import GymCards from "../../components/GymCards";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import { appliedJobs } from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
const AppliedJobs = (props) => {
  const [loading, setLoading] = useState(false);
  const [jobs, setJobs] = useState([]);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    setLoading(true);
    props.appliedJobs(user.email, setLoading);
  }, []);

  useEffect(() => {
    setJobs(props.applied_jobs);
  }, [props]);

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text
              style={{
                fontFamily: "MB",
                fontSize: rf(22),
                fontWeight: "700",
                left: 5,
              }}
            >
              Applied Jobs
            </Text>
          </View>

          {jobs?.length < 1 && !loading && <Text>No related job found</Text>}
          {loading ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            jobs?.map((item, index) => {
              return (
                <GymCards
                  favourite={item.favourites.includes(user.email)}
                  key={index}
                  status="applied"
                  data={item}
                />
              );
            })
          )}
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  title: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  applied_jobs: state.main.applied_jobs,
});
export default connect(mapStateToProps, { appliedJobs, logout })(AppliedJobs);
