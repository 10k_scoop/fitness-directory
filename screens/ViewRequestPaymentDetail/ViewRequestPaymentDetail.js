import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Platform,
  Linking,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { connect } from "react-redux";
import * as firebase from "firebase";
import AdminBottomMenu from "../../components/AdminBottomMenu";


const ViewRequestPaymentDetail = (props) => {

  const [loading, setLoading] = useState(false);
  var paymentIntent = props.route.params.data?.data;
  const user = firebase.auth().currentUser;
    let removalData={
        notiId:props.route.params?.notiId,
        reqPaymentId:props.route.params?.reqPaymentId,
    }
  const onRequestPayment = async () => {
    setLoading(true);
    props.navigation.navigate("PayRequestPaymentMethod",{data:paymentIntent,removalData:removalData})
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />

      <ScrollView>
        <View style={styles.ProfileContainer}>
          <View style={styles.BottomWidth}>
            <View style={styles.ProfilePicBody}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={{ uri: props.get_user_details?.profile }}
                resizeMode="cover"
              />
            </View>
            <View style={styles.ProfileNAmeBody}>
              <Text style={styles.Text}>{paymentIntent?.recruiterEmail}</Text>
              <Text style={styles.SecondaryText}>
                Job Type:-{paymentIntent?.jobDetails?.title}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.JohnTxtWrapper}>
          <Text style={[styles.Text, { fontSize: rf(17), fontFamily: "MB" }]}>
            Applicant Details
          </Text>
          <Text style={styles.Text}>Name: {paymentIntent?.applicantDetails?.name}</Text>
          <Text style={styles.Text}>
            Email: {paymentIntent?.applicantDetails?.email}
          </Text>
          <Text style={styles.Text}>
            Contact: {paymentIntent?.applicantDetails?.contact}
          </Text>
          <Text style={[styles.Text, { fontSize: rf(17), fontFamily: "MB" }]}>
            Job Details
          </Text>
          <Text style={styles.Text}>Title: {paymentIntent?.jobDetails?.title}</Text>
          <Text style={styles.Text}>
            Description: {paymentIntent?.jobDetails?.describtion}
          </Text>
          <Text style={styles.Text}>
            Location: {paymentIntent?.jobDetails?.location}
          </Text>
          <Text style={styles.Text}>Salary: {paymentIntent?.jobDetails?.salary}</Text>
          <Text style={[styles.Text, { fontSize: rf(17), fontFamily: "MB" }]}>
            Payment Details
          </Text>
          <Text style={styles.Text}>
            Receipt Email : {paymentIntent?.recruiterEmail}
          </Text>
          <Text style={styles.Text}>
            Amount to be Paid: {paymentIntent?.total} GBP
          </Text>

          <View style={{ alignItems: "center", marginTop: 15 }}>
            <TouchableOpacity style={styles.HireBtn} onPress={onRequestPayment}>
              <Text
                style={{
                  fontFamily: "MB",
                  fontSize: 15,
                  fontWeight: "400",
                  color: "#fff",
                }}
              >
                Make payment
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      <AdminBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ProfileContainer: {
    height: hp("12%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
  },
  HireBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  ProfilePicBody: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  ProfileNAmeBody: {
    height: "100%",
    paddingHorizontal: "5%",
    justifyContent: "center",
  },
  BottomWidth: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#A59F9F",
    alignItems: "center",
  },
  JohnTxtWrapper: {
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    marginTop: 10,
  },
  ResumeTxt: {
    height: hp("4%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    marginBottom: 5,
  },
  Text: {
    fontSize: rf(12),
    color: "#222",
    fontFamily: "MM",
    marginVertical: 5,
  },
  SecondaryText: {
    fontSize: rf(11),
    fontWeight: "400",
    color: "#A59F9F",
    fontFamily: "MM",
  },
  ResumeContainer: {
    height: hp("18%"),
    paddingHorizontal: wp("5%"),
  },
  ExperienceTxtWrapper: {
    height: hp("10%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  paymentPopup: {
    alignItems: "center",
    justifyContent: "center",
    width: wp("90%"),
    height: hp("30%"),
    backgroundColor: "white",
    position: "absolute",
    zIndex: 9999,
    top: hp("40%"),
    left: wp("5%"),
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    padding: 10,
    elevation: 5,
    alignItems: "center",
  },
  paymentPopupPicker: {
    width: "100%",
    height: hp("5%"),
    borderWidth: 1,
    borderColor: "#e5e5e5",
    borderRadius: 5,
    justifyContent: "center",
    marginBottom: 10,
  },
  paymentPopupItemTitle: {
    textAlign: "left",
    width: "100%",
    marginBottom: 10,
    fontWeight: "bold",
    color: "black",
  },
  continuePaymentBtn: {
    width: "45%",
    height: hp("5%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    color: "white",
  },
  continuePaymentBtnText: {
    color: "white",
    fontSize: rf(12),
    fontWeight: "bold",
  },
  continuePaymentBtnWrapper: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  resumeViewWrapper: {
    width: wp("90%"),
    height: hp("70%"),
    position: "absolute",
    backgroundColor: "#E2B269",
    zIndex: 9999999,
    left: wp("5%"),
    top: hp("15%"),
    borderRadius: 10,
  },
  resumeCloseBtn: {
    zIndex: 99999999999999,
    position: "absolute",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { logout })(ViewRequestPaymentDetail);
