import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";

export default function GymCards(props) {
  console.log(props.data)
  return (
    <View style={styles.container}>
      {
        props.data?.accountStatus == "notapproved" &&
        <View style={[styles.status,{backgroundColor:props.data?.accountStatus == "notapproved"?"red":"green",}]}>
          <Text style={styles.statusText}>{props.data?.accountStatus == "notapproved"?"Not Approved":"Approved"}</Text>
        </View>
      }
      <View style={styles.FirstRow}>
        <View style={styles.profile}>
          <Image
            style={{ width: "100%", height: "100%" }}
            source={props.data?.profile ? { uri: props.data?.profile } : require("../../../assets/profile.jpg")}
            resizeMode="cover"
          />
        </View>
        <View style={styles.Detail}>
          <Text style={{ fontSize: rf(13), fontWeight: "700", color: "#fff" }}>
            {props.data?.name}
          </Text>
          <Text style={{ fontSize: rf(12), fontWeight: "700", color: "#fff" }}>
            lorem ipsum lorem....
          </Text>
        </View>
      </View>
      <View style={styles.SecondRow}>
        <View style={styles.date}>
          <Text style={{ fontSize: rf(12), fontWeight: "700", opacity: 0.5 }}>
            Joined: Sep 1
          </Text>
        </View>
        <View style={styles.Btn}>
          <TouchableOpacity
            style={styles.ApplyBtn}
            onPress={props.onViewPress}
          >
            <Text
              style={{ fontSize: rf(14), fontWeight: "700", color: "#fff" }}
            >
              View
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("18%"),
    backgroundColor: "#E8DFCB",
    borderRadius: 14,
    marginBottom: hp("2%"),
  },
  FirstRow: {
    width: "100%",
    height: "65%",
    backgroundColor: "#E2B269",
    borderRadius: 14,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  profile: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Detail: {
    width: "65%",
    height: "50%",
    justifyContent: "space-evenly",
    paddingHorizontal: 7,
  },
  Icon: {
    width: "22%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  SecondRow: {
    width: "100%",
    height: "35%",
    flexDirection: "row",
  },
  date: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Btn: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-end",
  },
  ApplyBtn: {
    width: "50%",
    height: "60%",
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
    marginRight: wp("5%"),
  },
  status:{
    position:'absolute',
    minWidth:wp('20%'),
    height:hp('3%'),
    borderRadius:100,
    zIndex:99999,
    right:10,
    top:10,
    alignItems:'center',
    justifyContent:'center',
    paddingHorizontal:10
  },
  statusText:{
    fontSize:rf(10),
    color:'#fff',
    fontWeight:'bold'
  }
});
