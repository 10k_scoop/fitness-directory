import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import AdminBottomMenu from "../../components/AdminBottomMenu";
import GymCards from "./components/GymCards";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { ActivityIndicator } from "react-native-paper";
import { getAllUsers } from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";

const AdminDashboard = (props) => {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [venueCount, setVenueCount] = useState(0);
  const [instructorCount, setInstructorCount] = useState(0);
  const [searchText, setSearchText] = useState("");
  const [activeOption, setActiveOption] = useState("venue");
  const user = firebase.auth().currentUser;

  useEffect(() => {
    setLoading(true);
    props.getAllUsers(setLoading);
  }, []);

  useEffect(() => {
    if (props?.get_all_users != null) {
      setUsers(props?.get_all_users);
      let venue = 0;
      let instructor = 0;
      props?.get_all_users.map((item, index) => {
        if (item.role == "venue") {
          venue = venue + 1;
        } else if (item.role == "instructor") {
          instructor = instructor + 1;
        } else {
        }
      });
      setVenueCount(venue);
      setInstructorCount(instructor);
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header onLogoutPress={() => props.logout()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.ButtonWrapper}>
            <View style={styles.VenuWrapper}>
              <TouchableOpacity
                style={[
                  styles.VenuBtn,
                  {
                    backgroundColor:
                      activeOption == "venue" ? "#E2B269" : "#A99D9D",
                  },
                ]}
                onPress={() => setActiveOption("venue")}
              >
                <Text
                  style={[
                    styles.BtnTxt,
                    { color: activeOption == "venue" ? "#fff" : "#222" },
                  ]}
                >
                  Venue
                </Text>
              </TouchableOpacity>
              <View style={styles.Circle}>
                <Text style={styles.CircleTxt}>{venueCount}</Text>
              </View>
            </View>
            <View style={styles.VenuWrapper}>
              <TouchableOpacity
                style={[
                  styles.InstructorBtn,
                  {
                    backgroundColor:
                      activeOption == "instructor" ? "#E2B269" : "#A99D9D",
                  },
                ]}
                onPress={() => setActiveOption("instructor")}
              >
                <Text
                  style={[
                    styles.InstructorTxt,
                    { color: activeOption == "instructor" ? "#fff" : "#222" },
                  ]}
                >
                  Instructor
                </Text>
              </TouchableOpacity>
              <View style={styles.Circle}>
                <Text style={styles.CircleTxt}>{instructorCount}</Text>
              </View>
            </View>
          </View>
          {users.map((item, index) => {
            if (activeOption == "venue") {
              if (item.role == "venue") {
                return (
                  <GymCards
                    key={index}
                    data={item}
                    onViewPress={() =>
                      props.navigation.navigate("ViewUserProfile", {
                        data: item,
                        type: "admin",
                      })
                    }
                  />
                );
              }
            } else if (activeOption == "instructor") {
              if (item.role == "instructor") {
                return (
                  <GymCards
                    key={index}
                    data={item}
                    onViewPress={() =>
                      props.navigation.navigate("ViewApproval", {
                        data: item,
                        type: "admin",
                      })
                    }
                  />
                );
              }
            } else {
            }
          })}
        </View>
      </ScrollView>
      <AdminBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("10%"),
  },
  ButtonWrapper: {
    width: wp("100%"),
    height: hp("18%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  VenuBtn: {
    width: wp("40%"),
    height: hp("5%"),
    backgroundColor: "#E2B269",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  Circle: {
    width: hp("6%"),
    height: hp("6%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    marginTop: hp("1%"),
    alignItems: "center",
    justifyContent: "center",
  },
  VenuWrapper: {
    alignItems: "center",
  },
  CircleTxt: {
    fontSize: rf(16),
    fontWeight: "700",
    color: "#fff",
  },
  BtnTxt: {
    fontSize: rf(16),
    color: "#fff",
  },
  InstructorBtn: {
    width: wp("40%"),
    height: hp("5%"),
    backgroundColor: "#A99D9D",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
  },
  InstructorTxt: {
    fontSize: rf(16),
    color: "#222",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
  get_all_users: state.main.get_all_users,
});
export default connect(mapStateToProps, { logout, getAllUsers })(
  AdminDashboard
);
