import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Picker } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import TextInputField from "../../../components/TextInputField";
import GenderPicker from "./GenderPicker";
import { AntDesign, Feather } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Button from "../../../components/Button";
import DateTimePicker from "@react-native-community/datetimepicker";
import * as Location from "expo-location";

const Form = (props) => {
  const [name, setName] = useState("");
  const [venueName, setVenueName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [address, setAddress] = useState("");
  const [gender, setGender] = useState("male");
  const [dateOfBirth, setDateOfBirth] = useState(new Date());
  const [contact, setContact] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [isTerms, setIsTerms] = useState("");
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const [nationalInsuranceNumber, setNationalInsuranceNumber] = useState("");
  const [sortCode, setSortCode] = useState("");
  const [bankName, setBankName] = useState("");
  const [accountName, setAccountName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");
  const onChange = (event, selectedDate) => {
    console.log(selectedDate);
    const currentDate = selectedDate || dateOfBirth;
    setShow(Platform.OS === "ios");
    setDateOfBirth(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  const onSubmit = () => {
    let data = {
      email: email.toLowerCase(),
      password: pass,
      name: name,
      gender: gender,
      contact: contact,
      dateOfBirth: dateOfBirth,
      venueName: venueName,
      role: "venue",
      accountStatus: "notapproved",
      location: location,
      nationalInsuranceNumber:nationalInsuranceNumber,
      sortCode:sortCode,
      address:address,
      bankName: bankName,
      accountName: accountName,
      accountNumber: accountNumber,
      profile:
        "https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png",
    };

    if (
      email != "" &&
      pass != "" &&
      confirmPassword != "" &&
      isTerms &&
      name != "" &&
      bankName != "" &&
      accountName != "" &&
      accountNumber != "" &&
      address != "" &&
      sortCode != "" &&
      nationalInsuranceNumber != ""
    ) {
      if (pass == confirmPassword) {
        props.onSubmit(data);
      } else {
        alert("Password doesn't match");
      }
    } else {
      alert("Fill all details");
    }
  };

  return (
    <View style={styles.NameWrapper}>
      <TextInputField
        placeholder="Name"
        type="default"
        onChangeText={(val) => setName(val)}
      />
      <TextInputField
        placeholder="Venue Name"
        type="default"
        onChangeText={(val) => setVenueName(val)}
      />
      <TextInputField
        placeholder="Email"
        type="email-address"
        onChangeText={(val) => setEmail(val)}
      />
      <GenderPicker
        gender={gender}
        onMalePress={() => setGender("male")}
        onFeMalePress={() => setGender("female")}
      />
      <View style={styles.ContactBody}>
        <View style={{ width: "60%" }}>
          {show ? (
            <DateTimePicker
              testID="dateTimePicker"
              value={dateOfBirth}
              mode={mode}
              display="calendar"
              onChange={onChange}
            />
          ) : (
            <Text style={{ color: "#fff" }}>
              {dateOfBirth.getDate() +
                "-" +
                dateOfBirth.getMonth() +
                "-" +
                dateOfBirth.getFullYear() || "Date of Birth"}
            </Text>
          )}
        </View>
        <TouchableOpacity onPress={showDatepicker}>
          <AntDesign name="calendar" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
      <TextInputField
        placeholder="Contact"
        type="number-pad"
        onChangeText={(val) => setContact(val)}
      />
      <TextInputField
        placeholder="Address"
        type="text"
        onChangeText={(val) => setAddress(val)}
      />
      <TextInputField
        placeholder="Password"
        type="default"
        secureTextEntry
        onChangeText={(val) => setPass(val)}
      />
      <TextInputField
        placeholder="Confirm Password"
        type="default"
        secureTextEntry
        onChangeText={(val) => setConfirmPassword(val)}
      />
      <Text
        style={{
          fontSize: rf(16),
          color: "#fff",
          marginVertical: 10,
          fontWeight: "bold",
        }}
      >
        Add Bank Details
      </Text>
      <TextInputField
        placeholder="National insurance number"
        type="default"
        onChangeText={(val) => setNationalInsuranceNumber(val)}
      />
      <TextInputField
        placeholder="Bank Name"
        type="default"
        onChangeText={(val) => setBankName(val)}
      />
      <TextInputField
        placeholder="Account Name"
        type="default"
        onChangeText={(val) => setAccountName(val)}
      />
      <TextInputField
        placeholder="Account Number or IBAN"
        type="number-pad"
        onChangeText={(val) => setAccountNumber(val)}
      />
      <TextInputField
        placeholder="Sort Code"
        type="text"
        onChangeText={(val) => setSortCode(val)}
      />
      {/* Terms and conditions */}
      <View style={styles.TermsBody}>
        <TouchableOpacity onPress={() => setIsTerms(!isTerms)}>
          {isTerms ? (
            <AntDesign name="checksquare" size={rf(15)} color="#fff" />
          ) : (
            <Feather name="square" size={rf(15)} color="#fff" />
          )}
        </TouchableOpacity>
        <Text style={styles.termsText}>I agree to the Terms & Conditions</Text>
      </View>
      <View style={styles.BtnView}>
        <Button BtnText="Signup" onPress={onSubmit} />
      </View>
      <View style={styles.TxtLogin}>
        <Text style={styles.Text}>Already have an account? </Text>
        <TouchableOpacity onPress={props.onLoginPress}>
          <Text style={styles.Text}> Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  NameWrapper: {
    marginBottom: 20,
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
  ContactBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Pickerr: {
    color: "#fff",
    fontSize: rf(10),
    flex: 1,
  },
  TermsTextWrapper: {
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
  },
  TermsBody: {
    width: "100%",
    paddingHorizontal: wp("10%"),
    flexDirection: "row",
    alignItems: "center",
    marginVertical: hp("2%"),
  },

  BtnView: {
    height: hp("10%"),
    justifyContent: "center",
    alignItems: "center",
  },
  TxtLogin: {
    width: wp("100%"),
    justifyContent: "center",
    flexDirection: "row",
  },
  termsText: {
    fontWeight: "600",
    fontSize: rf(11),
    color: "#fff",
    marginLeft: "5%",
  },

  Text: {
    fontWeight: "600",
    fontSize: rf(12),
    color: "#fff",
  },
  datePicker: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: 9999999,
  },
});

export default Form;
