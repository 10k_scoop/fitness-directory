import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Image,
  ScrollView,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
  ActivityIndicator,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import Form from "./component/Form";
import { signup } from "../../state-management/actions/auth/FirebaseAuthActions";
const VenueSignup = (props) => {
  const [loading, setLoading] = useState(false);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  const onFormSubmit = (data) => {
    setLoading(true);
    props.signup(data, setLoading);
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
        >
          <ScrollView>
            <View style={styles.LogoBody}>
              <Image
                style={{ height: "70%", width: "70%" }}
                source={require("../../assets/logo.png")}
                resizeMode="cover"
              />
            </View>

            <View style={styles.formWrapper}>
              <Form
                onSubmit={(data) => onFormSubmit(data)}
                onLoginPress={() => navigation.navigate("Login")}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  PicWrapper: {
    height: hp("100%"),
    width: wp("100%"),
    paddingTop:
      Platform.OS == "ios"
        ? StatusBar.currentHeight + hp("5%")
        : StatusBar.currentHeight + hp("1%"),
  },
  Layer: {
    height: hp("100%"),
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    height: hp("27%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  LogoBody: {
    height: hp("20%"),
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  formWrapper: {
    alignItems: "center",
    justifyContent: "center",
    marginVertical: hp("3%"),
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_details: state.main.signup_details,
});
export default connect(mapStateToProps, { signup })(VenueSignup);
