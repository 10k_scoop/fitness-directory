import React from "react";
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function ChooseOption({ navigation }) {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <View style={styles.LogoWrapper}>
          <View style={styles.LogoBody}>
            <Image
              style={{ height: "80%", width: "90%" }}
              source={require("../../assets/logo.png")}
              resizeMode="cover"
            />
          </View>
        </View>

        <View style={styles.secondCont}>
          <Text style={styles.optionsTitle}>Choose your role</Text>
          <View style={styles.optionsWrapper}>
            <TouchableOpacity
              style={styles.option}
              onPress={() => navigation.navigate("Login", { role: "venue" })}
            >
              <View style={styles.optionLayer}></View>
              <Text style={styles.optionsText}>Venue</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.option}
              onPress={() =>
                navigation.navigate("Login", { role: "instructor" })
              }
            >
              <View style={styles.optionLayer}></View>
              <Text style={styles.optionsText}>Instructor</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  PicWrapper: {
    height: "100%",
    width: wp("100%"),
  },
  Layer: {
    height: "100%",
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    width: wp("100%"),
    justifyContent: "flex-end",
    bottom: hp("5%"),
  },
  LogoBody: {
    height: "50%",
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  optionsWrapper: {
    justifyContent: "space-around",
    flexDirection: "row",
  },
  option: {
    width: wp("30%"),
    height: wp("30%"),
    borderRadius: wp("20%"),
    borderWidth: 2,
    borderColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  optionLayer: {
    backgroundColor: "#E2B269",
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: wp("20%"),
    opacity: 0.7,
  },
  optionsText: {
    fontSize: rf(16),
    color: "#fff",
    fontWeight: "bold",
    fontFamily: "MS",
  },
  optionsTitle: {
    textAlign: "center",
    fontSize: rf(22),
    color: "#fff",
    marginVertical: hp("6%"),
    fontFamily: "MR",
  },
  secondCont: {
    flex: 1,
    bottom: hp("5%"),
  },
});
