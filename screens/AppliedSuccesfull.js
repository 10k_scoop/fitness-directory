import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import BottomMenu from "../components/BottomMenu";
import { Ionicons } from "@expo/vector-icons";

export default function AppliedSuccesfull({ navigation }) {
  return (
    <View style={styles.container}>
      <Header />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.SubmitBox}>
            <View style={styles.Icon}>
              <Ionicons name="checkmark-sharp" size={rf(50)} color="#fff" />
            </View>
            <Text style={{ fontSize: rf(20), fontWeight: "700" }}>
              Your applicaton has{" "}
            </Text>
            <Text style={{ fontSize: rf(20), fontWeight: "700" }}>
              been submitted{" "}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.ContinueBtn}
            onPress={() => navigation.navigate("InstructorDashboard")}
          >
            <Text
              style={{ fontSize: rf(16), fontWeight: "700", color: "#fff" }}
            >
              Continue
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.BackBtn}
            onPress={() => navigation.navigate("SearchJobs")}
          >
            <Text
              style={{ fontSize: rf(16), fontWeight: "700", color: "#E2B269" }}
            >
              Find more similar jobs.
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  SubmitBox: {
    width: wp("90%"),
    height: hp("30%"),
    backgroundColor: "#F7F7F7",
    borderRadius: 10,
    marginTop: "15%",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "25%",
  },
  Icon: {
    width: hp("10%"),
    height: hp("10%"),
    borderRadius: 100,
    backgroundColor: "#3CD016",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "5%",
  },
  ContinueBtn: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "#E2B269",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "7%",
  },
  BackBtn: {
    width: wp("90%"),
    height: hp("6%"),
    borderWidth: 1,
    borderColor: "#E2B269",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
  },
});
