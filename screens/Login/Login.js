import React, { useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import TextInputField from "../../components/TextInputField";
import Button from "../../components/Button";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { login } from "../../state-management/actions/auth/FirebaseAuthActions";
import { TouchableOpacity } from "react-native-gesture-handler";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [loading, setLoading] = useState(false);

  const onSubmit = () => {};

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <View style={styles.LogoWrapper}>
          <View style={styles.LogoBody}>
            <Image
              style={{ height: "80%", width: "90%" }}
              source={require("../../assets/logo.png")}
              resizeMode="cover"
            />
          </View>
        </View>
        <View style={styles.MailWrapper}>
          <TextInputField
            placeholder="Enter Your Email"
            onChangeText={(val) => setEmail(val)}
            type="email-address"
          />
          <TextInputField
            placeholder="Enter Your Password"
            onChangeText={(val) => setPass(val)}
            type="default"
            secureTextEntry
          />
        </View>
        <View
          style={{ alignItems: "flex-end", width: wp("90%"), marginTop: 10 }}
        >
          <TouchableOpacity
            onPress={() => props.navigation.navigate("ForgotPassword")}
          >
            <Text
              style={{ fontFamily: "MS", color: "#fff", textAlign: "center" }}
            >
              Forgot Password ?
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.BtnWrapper}>
          <Button
            BtnText={
              loading ? (
                <ActivityIndicator size="small" color="#fff" />
              ) : (
                "Login"
              )
            }
            onPress={() => {
              loading ? undefined : setLoading(true);
              props.login(email, pass, setLoading);
            }}
          />
          <Text
            style={{
              fontFamily: "MB",
              fontSize: rf(13),
              color: "#fff",
              fontWeight: "900",
            }}
          >
            OR
          </Text>
          <Button
            BtnText="Signup"
            onPress={() =>
              props.route.params.role == "venue"
                ? props.navigation.navigate("VenueSignup")
                : props.navigation.navigate("InstructorSignup")
            }
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  PicWrapper: {
    height: hp("100%"),
    width: wp("100%"),
  },
  Layer: {
    height: hp("100%"),
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    justifyContent: "flex-end",
  },
  LogoBody: {
    height: "50%",
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  MailWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  BtnWrapper: {
    height: hp("25%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  login_details: state.main.login_details,
});
export default connect(mapStateToProps, { login })(Login);
