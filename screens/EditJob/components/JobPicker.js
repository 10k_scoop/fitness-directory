import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Picker } from "@react-native-picker/picker";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function JobPicker(props) {
  return (
    <View>
      <View style={styles.SkilCategoryBody}>
        <Text style={{ fontWeight: "400", fontSize: rf(11), color: "#fff" }}>
          {props.Title}
        </Text>
        <View style={{ flex: 1 }}>
          <Picker
            style={styles.Pickerr}
            selectedValue={props.hookType}
            onValueChange={props.onValueChange}
            itemStyle={{ color: "#fff", fontSize: rf(13) }}
          >
            <Picker.Item
              label="select"
              value={props.value ? props.value : null}
            />

            {props.items.map((item, index) => {
              return (
                <Picker.Item
                  key={index}
                  label={item.label}
                  value={item.value}
                />
              );
            })}
          </Picker>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  SkilCategoryBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#E2B269",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 100,
    marginBottom: 20,
    paddingHorizontal: 20,
    overflow: "hidden",
  },
});
