import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import Skils from "./components/Skills";
import JobPicker from "./components/JobPicker";
import PostJobTextField from "./components/PostJobTextField";
import { TextInput } from "react-native-gesture-handler";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import { updateJob } from "../../state-management/actions/Features/Actions";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";

const EditJob = (props) => {
  const [jobTitle, setJobTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [location, setLocation] = useState("");
  const [totalPositions, setTotalPositions] = useState("");
  const [jobType, setJobType] = useState("");
  const [jobShift, setJobShift] = useState("");
  const [gender, setGender] = useState("");
  const [experience, setExperience] = useState("");
  const [skillText, setSkillText] = useState("");
  const [salary, setSalary] = useState("");
  const [loading, setLoading] = useState(false);
  const [skills, setSkills] = useState([]);

  let prevData = props.route.params?.data;

  useEffect(() => {
    setJobTitle(prevData.title);
    setDesc(prevData.describtion);
    setLocation(prevData.location);
    setTotalPositions(prevData.totalPositions);
    setJobType(prevData.jobType);
    setJobShift(prevData.jobShift);
    setGender(prevData.gender);
    setExperience(prevData.experience);
    setSalary(prevData.salary);
    setSkills(prevData.skills);
    console.log(prevData);
  }, []);
  const onAddSkill = () => {
    if (skillText != "") {
      setSkills((prevstate) => [...prevstate, skillText]);
      setSkillText("");
    }
  };
  const onRemoveSkill = (value) => {
    let filteredArray = skills.filter((item) => item !== value);
    setSkills(filteredArray);
  };
  const user = firebase.auth().currentUser;
  const onSubmit = () => {
    let data = {
      title: jobTitle,
      describtion: desc,
      location: location,
      skills: skills,
      totalPositions: totalPositions,
      jobType: jobType,
      jobShift: jobShift,
      gender: gender,
      experience: experience,
      salary: salary,
      thumbnail: "",
      author: user.email,
      id: prevData.id,
    };
    setLoading(true);
    props.updateJob(data, setLoading);
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <View style={styles.FieldWrapper}>
            <View>
              <PostJobTextField
                placeholder="Job Title"
                onChangeText={(val) => setJobTitle(val)}
                value={jobTitle}
              />
            </View>
            <View style={styles.JobDescriptionBody}>
              <TextInput
                style={styles.DescriptionTxt}
                placeholder="Lorem Ipsum"
                multiline={true}
                placeholderTextColor="#fff"
                onChangeText={(val) => setDesc(val)}
                value={desc}
              />
            </View>
            <View>
              <PostJobTextField
                placeholder="Location"
                onChangeText={(val) => setLocation(val)}
                value={location}
              />
            </View>

            <View style={styles.SkillsBody}>
              <TextInput
                placeholderTextColor="#fff"
                style={styles.SkillsTxt}
                value={skillText}
                placeholder="Add Skill"
                onChangeText={(val) => setSkillText(val.toLowerCase())}
              />
              <TouchableOpacity onPress={onAddSkill}>
                <AntDesign name="pluscircleo" size={24} color="#fff" />
              </TouchableOpacity>
            </View>
            <View style={styles.SkilCategory}>
              {skills.map((item, index) => {
                return (
                  <Skils
                    Skill={item}
                    key={index}
                    onRemove={() => onRemoveSkill(item)}
                  />
                );
              })}
            </View>
            <View>
              <PostJobTextField
                placeholder="Total Positions"
                onChangeText={(val) => setTotalPositions(val)}
                value={totalPositions}
              />
            </View>

            <View style={styles.PickerWrapper}>
              <JobPicker
                Title="Job Type"
                hookType={jobType == "fulltime" ? "Full Time" : "Part Time"}
                onValueChange={(val) => setJobType(val)}
                items={[
                  { label: "Part Time", value: "Part Time" },
                  { label: "Full Time", value: "Full Time" },
                ]}
              />
              <JobPicker
                Title="Job Shift"
                hookType={jobShift == "morning" ? "Morning" : "Evening"}
                onValueChange={(val) => setJobShift(val)}
                items={[
                  { label: "Morning", value: "Morning" },
                  { label: "Evening", value: "Evening" },
                ]}
              />
              <JobPicker
                Title="Gender"
                hookType={gender == "female" ? "Female" : "Male"}
                onValueChange={(val) => setGender(val)}
                items={[
                  { label: "Male", value: "Male" },
                  { label: "Female", value: "Female" },
                ]}
              />
              <JobPicker
                Title="Experience"
                hookType={experience}
                onValueChange={(val) => setExperience(val)}
                items={[
                  { label: "1-2", value: "1-2" },
                  { label: "3-5", value: "3-5" },
                  { label: "5+", value: "5+" },
                ]}
              />
              <PostJobTextField
                placeholder="Salary"
                value={salary}
                onChangeText={(val) => setSalary(val)}
              />
            </View>
            <View style={styles.SubmitBtnWrapper}>
              <TouchableOpacity style={styles.SubBtn} onPress={onSubmit}>
                <Text
                  style={{ fontSize: rf(11), fontWeight: "400", color: "#fff" }}
                >
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  FieldWrapper: {
    width: wp("100%"),
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    marginTop: 10,
    marginBottom: hp("15%"),
  },
  JobDescriptionBody: {
    height: hp("20%"),
    width: "100%",
    backgroundColor: "#D4A35A",
    borderRadius: 20,
    padding: "5%",
    marginBottom: 20,
  },
  DescriptionTxt: {
    fontSize: rf(11),
    fontWeight: "400",
    height: "100%",
    width: "100%",
    color: "#fff",
    textAlignVertical: "top",
  },
  SkillsBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  SkillsTxt: {
    fontSize: rf(11),
    fontWeight: "400",
    height: "100%",
    width: "90%",
    color: "#fff",
  },
  SkilCategory: {
    flexWrap: "wrap",
    flexDirection: "row",
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  PickerWrapper: {
    alignItems: "center",
  },
  SubmitBtnWrapper: {
    height: hp("15%"),
    alignItems: "center",
    justifyContent: "center",
    bottom: hp("5%"),
  },
  SubBtn: {
    height: hp("5%"),
    width: wp("80%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  update_job: state.main.update_job,
});
export default connect(mapStateToProps, { updateJob, logout })(EditJob);
