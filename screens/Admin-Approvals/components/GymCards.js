import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";

export default function GymCards(props) {
  return (
    <View style={styles.container}>
      <View style={styles.FirstRow}>
        <View style={styles.profile}>
          <Image
            style={{ width: "100%", height: "100%" }}
            source={require("../../../assets/profile.jpg")}
            resizeMode="cover"
          />
        </View>
        <View style={styles.Detail}>
          <Text style={styles.font1}>{props.data.name}</Text>
          <Text style={styles.font2}>{props.data.role}</Text>
        </View>
        <View style={styles.circleWrapper}>
          <View style={styles.circle}>
            <Text style={styles.circleTxt}>{props.title}</Text>
          </View>
        </View>
      </View>
      <View style={styles.SecondRow}>
        <View style={styles.date}>
          <Text style={{ fontSize: rf(12), fontWeight: "700", opacity: 0.5 }}>
            Joined: Sep 1
          </Text>
        </View>
        <View style={styles.Btn}>
          <TouchableOpacity
            style={styles.ApplyBtn}
            onPress={props.onApplyClick}
            
          >
            <Text style={styles.font3}>View</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("18%"),
    backgroundColor: "#E8DFCB",
    borderRadius: 14,
    marginBottom: hp("2%"),
  },
  FirstRow: {
    width: "100%",
    height: "65%",
    backgroundColor: "#E2B269",
    borderRadius: 14,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  profile: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Detail: {
    width: "65%",
    height: "50%",
    justifyContent: "space-evenly",
    paddingHorizontal: 7,
  },
  Icon: {
    width: "22%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  SecondRow: {
    width: "100%",
    height: "35%",
    flexDirection: "row",
  },
  date: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Btn: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    alignItems: "flex-end",
  },
  ApplyBtn: {
    width: "50%",
    height: "60%",
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
    marginRight: wp("5%"),
  },
  circleWrapper: {
    width: "30%",
    height: "100%",
    alignItems: "center",
  },
  circle: {
    width: hp("3.5%"),
    height: hp("3.5%"),
    borderRadius: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 8,
  },
  circleTxt: {
    fontSize: rf(15),
    fontWeight: "700",
  },
  font1: {
    fontSize: rf(13),
    fontWeight: "700",
    color: "#fff",
  },
  font2: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "#fff",
  },
  font3: {
    fontSize: rf(14),
    fontWeight: "700",
    color: "#fff",
  },
});
