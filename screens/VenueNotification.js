import React, { useEffect,useState } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import NotificationCard from "../components/NotificationCard";
import BottomMenu from "../components/BottomMenu";
import { connect } from "react-redux";
import VenueBottomMenu from "../components/VenueBottomMenu";
import { logout } from "../state-management/actions/auth/FirebaseAuthActions";
import { getAllNotifications } from "../state-management/actions/Features/Actions";
import * as firebase from "firebase";

const VenueNotification = (props) => {

  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;
  

  useEffect(() => {
    props.getAllNotifications(setLoading);
  }, []);

  useEffect(() => {
    setNotifications(props.get_all_notifications);
  }, [props]);
  const onNotificationPress=(data)=>{
    if(data?.data?.type=="payment"){
      props?.navigation.navigate("RequestedPayments",{data:data})
    }else if(data?.data?.type=="applied"){

    }else{
      
    }
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text
              style={{ fontFamily: "MB", fontSize: rf(22), fontWeight: "700" }}
            >
              Notifications
            </Text>
          </View>
          {notifications?.map((item, index) => {
            if(item?.data?.to==user.email){
              return <NotificationCard key={index} data={item} onPress={()=>onNotificationPress(item)} />;
            }
          })}
          <Text>No notifications</Text>
        </View>
      </ScrollView>
      <VenueBottomMenu navigation={props.navigation} onPress={onNotificationPress} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "15%",
  },
  title: {
    width: wp("95%"),
    height: hp("8%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_all_notifications: state.main.get_all_notifications,
});
export default connect(mapStateToProps, { logout,getAllNotifications })(VenueNotification);
