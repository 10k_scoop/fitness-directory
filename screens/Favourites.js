import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import GymCards from "../components/GymCards";
import BottomMenu from "../components/BottomMenu";
import { connect } from "react-redux";
import {
  updateFavourite,
  getJobs,
} from "../state-management/actions/Features/Actions";
import { logout } from "../state-management/actions/auth/FirebaseAuthActions";
import * as firebase from "firebase";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const Favourites = (props) => {
  const [loading, setLoading] = useState(true);
  const [jobs, setJobs] = useState([]);
  const user = firebase.auth().currentUser;

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.getJobs(setLoading);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.getJobs(setLoading);
    return () => {
      setLoading(true);
    };
  }, []);

  useEffect(() => {
    setJobs(props?.get_jobs);
  }, [props]);

  const addToFavourites = (fav, id) => {
    if (fav.includes(user.email)) {
      fav.pop(user.email);
      let data = {
        favourites: fav,
        id: id,
      };
      props.updateFavourite(data);
    } else {
      fav.push(user.email);
      let data = {
        favourites: fav,
        id: id,
      };
      props.updateFavourite(data);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props?.logout()}
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text
              style={{ fontFamily: "MB", fontSize: rf(22), fontWeight: "700" }}
            >
              Favourites
            </Text>
          </View>

          {loading ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            jobs?.map((item, index) => {
              if (item?.favourites?.includes(user.email)) {
                return (
                  <GymCards
                    key={index}
                    status={
                      item.applicants?.includes(user.email)
                        ? "applied"
                        : "apply"
                    }
                    data={item}
                    onApplyClick={() =>
                      props.navigation.navigate("ViewJob", { data: item })
                    }
                    onHeartPress={() =>
                      addToFavourites(item.favourites, item.id)
                    }
                    favourite={true}
                  />
                );
              }
            })
          )}
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  title: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
  get_jobs: state.main.get_jobs,
});
export default connect(mapStateToProps, {
  logout,
  updateFavourite,
  getJobs,
})(Favourites);
