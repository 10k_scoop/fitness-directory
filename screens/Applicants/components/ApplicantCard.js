import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ApplicationCard(props) {
  return (
    <View style={styles.mainCont}>
      <View style={styles.container}>
        {/* Top Left Button */}
        <TouchableOpacity
          style={styles.barCardTopTag}
          onPress={props.onMenuPress}
        >
          <View style={styles.barCardTopTagTextWrapper}>
            <Text style={styles.barCardTopTagText}>Doesn't Match </Text>
          </View>
        </TouchableOpacity>
        {/* Top Left Button */}
        <View style={styles.FirstRow}>
          <View style={styles.profile}>
            <Image
              style={{ width: "100%", height: "100%" }}
              source={{ uri: props.data.applicantProfile }}
              resizeMode="cover"
            />
          </View>
          <View style={styles.Detail}>
            <Text
              style={{
                fontFamily: "MB",
                fontSize: rf(14),
                fontWeight: "700",
                color: "#E2B269",
              }}
            >
              {props?.data?.applicantName}
            </Text>
            <Text
              style={{
                fontFamily: "MM",
                fontSize: rf(12),
                fontWeight: "700",
                color: "#E2B269",
              }}
            >
              {props?.data?.title}
            </Text>
          </View>
        </View>
        <View style={styles.SecondRow}>
          <View style={styles.Btn}>
            <TouchableOpacity
              style={styles.ApplyBtn}
              onPress={props.onViewApplication}
            >
              <Text
                style={{
                  fontFamily: "MB",
                  fontSize: rf(12),
                  fontWeight: "700",
                  color: "#fff",
                }}
              >
                View Application
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainCont: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
    width: wp("100%"),
  },
  container: {
    width: wp("95%"),
    height: hp("18%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 14,
    marginBottom: hp("1.5%"),
    marginHorizontal: "2.5%",
    overflow: "hidden",
  },
  FirstRow: {
    width: "100%",
    height: "65%",
    backgroundColor: "#fff",
    borderRadius: 14,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
  },
  profile: {
    width: hp("8%"),
    height: hp("8%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  Detail: {
    width: "65%",
    height: "50%",
    justifyContent: "space-evenly",
    paddingHorizontal: 7,
  },
  Icon: {
    width: "22%",
    height: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  SecondRow: {
    width: "100%",
    height: "35%",
    flexDirection: "row",
  },
  date: {
    width: "50%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Btn: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  ApplyBtn: {
    width: "70%",
    height: "52%",
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
  },
  barCardTopTag: {
    width: wp("33%"),
    height: hp("3%"),
    borderRadius: wp("50%"),
    borderBottomLeftRadius: wp("5%"),
    backgroundColor: "#F5CA48",
    transform: [{ rotateX: "-180deg" }],
    right: -wp("5%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 99999,
  },
  barCardTopTagTextWrapper: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  barCardTopTagText: {
    fontSize: rf(11),
    fontWeight: "bold",
    transform: [{ rotateX: "-180deg" }],
    fontFamily: "MM",
  },
});
