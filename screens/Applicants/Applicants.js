import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import { connect } from "react-redux";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import {
  myApplications,
  viewJob,
} from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";
import ApplicationCard from "./components/ApplicantCard";
import { getUserData } from "../../state-management/actions/auth/FirebaseAuthActions";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const Applicants = (props) => {
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.myApplications(user.email, setLoading);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.myApplications(user.email, setLoading);
  }, []);

  if (loading) {
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator size="large" color="#222" />
    </View>;
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "700", left: 5 }}>
              Applications
            </Text>
          </View>
          {loading ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            props?.my_applications?.map((item, index) => {
              return (
                <ApplicationCard
                  onViewApplication={() =>
                    props.navigation.navigate("SingleApplication", {
                      data: item,
                    })
                  }
                  data={item?.data}
                  key={index}
                />
              );
            })
          )}
        </View>
      </ScrollView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  title: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  my_applications: state.main.my_applications,
  view_job: state.main.view_job,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  myApplications,
  viewJob,
  getUserData,
  logout,
})(Applicants);
