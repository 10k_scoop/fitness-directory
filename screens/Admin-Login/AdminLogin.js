import React from "react";
import { ImageBackground, StyleSheet, Text, View, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import TextInputField from "../../components/TextInputField";
import Button from "../../components/Button";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function AdminLogin({ navigation }) {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <View style={styles.LogoWrapper}>
          <View style={styles.LogoBody}>
            <Image
              style={{ height: "80%", width: "80%" }}
              source={require("../../assets/logo.png")}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.MailWrapper}>
          <TextInputField placeholder="Enter Your Email" />
          <TextInputField placeholder="Enter Your Password" />
        </View>
        <View style={styles.BtnWrapper}>
          <Button
            BtnText="Login"
            onPress={() => navigation.navigate("AdminDashboard")}
          />
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  PicWrapper: {
    height: hp("100%"),
    width: wp("100%"),
  },
  Layer: {
    height: hp("100%"),
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    justifyContent: "flex-end",
  },
  LogoBody: {
    height: "50%",
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  MailWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  BtnWrapper: {
    height: hp("25%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    justifyContent: "space-evenly",
  },
});
