import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Linking,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import ContactOption from "./component/ContactOption";
import ResumeUpload from "./component/ResumeUpload";
import ContactBtn from "./component/ContactBtn";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import { getUserData } from "../../state-management/actions/auth/FirebaseAuthActions";
import {
  approveUser,
  rejectUser,
} from "../../state-management/actions/Features/Actions";
import { connect } from "react-redux";
import MapView from "react-native-maps";
import AdminBottomMenu from "../../components/AdminBottomMenu";
import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";

const ViewUserProfile = (props) => {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState();
  const [status, setStatus] = useState();
  const [showBankDetails, setShowBankDetails] = useState(false);

  var data = props.route.params.data;

  useEffect(() => {
    setStatus(props.route.params?.data?.accountStatus);
  }, []);

  const onApprove = () => {
    setLoading(true);
    props.approveUser(data.email, setLoading);
    setStatus("approved");
  };
  const onReject = () => {
    setLoading(true);
    props.rejectUser(data?.email, setLoading,props?.navigation);
    setStatus("notapproved");
  };
  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }
  const onCallClick = () => {
    let phoneNumber = data?.contact;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${data?.contact}`;
    } else {
      phoneNumber = `tel:${data?.contact}`;
    }
    Linking.openURL(phoneNumber);
  };
  return (
    <View style={styles.container}>
      {/* Bank Details Viewer */}
      {showBankDetails && (
        <View style={styles.bankDetailsPopup}>
          <TouchableOpacity
            style={[styles.resumeCloseBtn, { left: -10, top: -10 }]}
            onPress={() => setShowBankDetails(!setShowBankDetails)}
          >
            <FontAwesome5 name="times-circle" size={30} color="#222" />
          </TouchableOpacity>
          <View style={styles.bodyHeader}>
            <FontAwesome name="bank" size={rf(30)} color="#fff" />
            <Text style={styles.headerText}>Bank details</Text>
          </View>
          <Text style={styles.bankDetailsVariablesText}>
            National Insurance Number: {"\n"}
            {data?.nationalInsuranceNumber}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Sort Code: {"\n"}
            {data?.sortCode}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Bank Name: {"\n"}
            {data?.bankName}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Account Name: {"\n"}
            {data?.accountName}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Account no or IBAN: {"\n"} {data?.accountNumber}
          </Text>
        </View>
      )}
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() =>
          props.route.params?.type == "admin"
            ? props.navigation.navigate("AdminDashboard")
            : props.navigation.navigate("VenueDashboard")
        }
      />

      <ScrollView>
        <View style={styles.ProfileContainer}>
          <View style={styles.BottomWidth}>
            <View style={styles.ProfilePicBody}>
              <Image
                style={{ height: "100%", width: "100%" }}
                source={{ uri: data.profile }}
                resizeMode="cover"
              />
            </View>
            <View style={styles.ProfileNAmeBody}>
              <Text style={styles.Text}>
                {data?.role == "venue" ? data.venueName : data.name}
              </Text>
              <Text style={styles.SecondaryText}>{data?.role}</Text>
            </View>
          </View>
        </View>
        <View style={styles.JohnTxtWrapper}>
          <Text style={styles.Text}>{data?.name}</Text>
          <Text style={styles.SecondaryText}>{data?.email}</Text>
        </View>
        <ContactOption
          onCallPress={() => onCallClick()}
          onMailPress={() =>
            Linking.openURL(
              `mailto:${data?.email}?subject=SendMail&body=Description`
            )
          }
          onBankPress={() => setShowBankDetails(!showBankDetails)}
        />
        {data?.role == "venue" && (
          <View style={styles.venueMap}>
            <Text style={[styles.Text, { marginBottom: 15, left: 10 }]}>
              Venue Current Location
            </Text>
            <MapView
              style={styles.map}
              region={{
                latitude: data?.location?.coords?.latitude,
                longitude: data?.location?.coords?.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
            />
          </View>
        )}
        {data?.role == "instructor" && (
          <View style={styles.ExperienceTxtWrapper}>
            {user?.skills?.map((item, index) => {
              return (
                <Text
                  key={index}
                  style={{
                    fontFamily: "MR",
                    fontSize: rf(11),
                    fontWeight: "400",
                    marginVertical: 4,
                  }}
                >
                  {item}
                </Text>
              );
            })}
          </View>
        )}

        <View style={styles.BtnContainer}>
          <TouchableOpacity style={styles.HireBtn} onPress={() => onApprove()}>
            <Text
              style={{
                fontFamily: "MB",
                fontSize: 15,
                fontWeight: "400",
                color: "#fff",
              }}
            >
              Approve User
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.rejectBtn} onPress={() => onReject()}>
            <Text
              style={{
                fontFamily: "MB",
                fontSize: 15,
                fontWeight: "400",
                color: "#fff",
              }}
            >
              Reject User
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      {props.route.params?.type == "admin" ? (
        <AdminBottomMenu navigation={props.navigation} />
      ) : (
        <VenueBottomMenu navigation={props.navigation} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  BtnContainer: {
    height: hp("4%"),
    justifyContent: "center",
    paddingHorizontal: wp("10%"),
    alignItems: "center",
    marginTop:hp('12%')
  },
  HireBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom:hp('2%')
  },
  rejectBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#cf2129",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom:hp('2%')
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  ProfileContainer: {
    height: hp("12%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
  },
  ProfilePicBody: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
  },
  ProfileNAmeBody: {
    height: "100%",
    paddingHorizontal: "5%",
    justifyContent: "center",
  },
  BottomWidth: {
    flex: 1,
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#A59F9F",
    alignItems: "center",
  },
  JohnTxtWrapper: {
    height: hp("7%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  ResumeTxt: {
    height: hp("4%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
    marginBottom: 5,
  },
  Text: {
    fontSize: rf(12),
    fontWeight: "500",
    color: "#222",
    fontFamily: "MB",
  },
  SecondaryText: {
    fontSize: rf(11),
    fontWeight: "400",
    color: "#A59F9F",
    fontFamily: "MM",
  },
  ResumeContainer: {
    height: hp("18%"),
    paddingHorizontal: wp("5%"),
  },
  ExperienceTxtWrapper: {
    height: hp("10%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "center",
  },
  venueMap: {
    width: "90%",
    height: hp("20%"),
    borderRadius: 10,
    marginHorizontal: wp("5%"),
    overflow: "hidden",
  },
  map: {
    width: "100%",
    height: "100%",
  },
  bankDetailsPopup: {
    width: wp("80%"),
    minHeight: hp("40%"),
    position: "absolute",
    backgroundColor: "#E2B269",
    zIndex: 9999999999,
    left: wp("10%"),
    top: hp("35%"),
    borderRadius: 10,
    padding: hp("2%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    borderWidth: 2,
    elevation: 9,
    borderColor: "#fff",
  },
  bodyHeader: {
    marginBottom: hp("0%"),
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    fontSize: rf(20),
    fontWeight: "700",
    marginVertical: wp("2%"),
    color: "#fff",
  },
  bankDetailsVariablesText: {
    fontSize: rf(15),
    fontWeight: "700",
    marginVertical: wp("1%"),
    color: "#222",
    textAlign: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  logout,
  getUserData,
  approveUser,
  rejectUser,
})(ViewUserProfile);
