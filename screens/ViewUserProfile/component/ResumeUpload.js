import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function ResumeUpload({ image }) {
  return (
    <View style={styles.ResumeBody}>
      <View style={styles.ResumeViewer}>
        <View style={styles.ResumeInnerBody}>
          <Image
            style={{ height: "100%", width: "100%" }}
            source={{ uri: image }}
            resizeMode="cover"
          />
        </View>
      </View>
      <View style={styles.BtnResumeConatiner}>
        <TouchableOpacity style={styles.BtnResume}>
          <Text style={{ fontFamily: "MM", fontSize: rf(12) }}>
            View Resume
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  ResumeBody: {
    flex: 1,
    backgroundColor: "#E2B269",
    borderRadius: 15,
  },
  ResumeViewer: {
    flex: 0.6,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 5,
  },
  ResumeInnerBody: {
    height: hp("7%"),
    width: wp("15%"),
    overflow: "hidden",
    borderRadius: 5,
    left: hp("3%"),
  },
  BtnResumeConatiner: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "center",
  },
  BtnResume: {
    height: "100%",
    width: "80%",
    backgroundColor: "#fff",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
