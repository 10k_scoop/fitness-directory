import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { getRequestPayments } from "../../state-management/actions/Features/Actions";
import * as firebase from "firebase";
import PaymentCard from "./components/PaymentCard";

const RequestPayments = (props) => {
  const [payments, setPayments] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    props.getRequestPayments(setLoading);
  }, []);

  useEffect(() => {
    setPayments(props.get_all_request_payments);
  }, [props]);

  const onView=(data,reqPaymentId)=>{
      props.navigation.navigate("ViewRequestPaymentDetail",{data:data,notiId:props.route.params?.data?.id,reqPaymentId:reqPaymentId})
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text
              style={{ fontFamily: "MB", fontSize: rf(22), fontWeight: "700" }}
            >
              Pending Payments
            </Text>
          </View>
          {payments?.map((item, index) => {
            return item?.data?.recruiterEmail==user.email?<PaymentCard onPress={(data)=>onView(data,item.id)} userDetails={props?.get_user_details} key={index} data={item} />:null;
          })}
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "15%",
  },
  title: {
    width: wp("95%"),
    height: hp("8%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_all_request_payments: state.main.get_all_request_payments,
  get_user_details:state.main.get_user_details
});
export default connect(mapStateToProps, { logout, getRequestPayments })(
  RequestPayments
);
