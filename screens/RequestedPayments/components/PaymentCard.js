import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function PaymentCard(props) {
  return (
    <View style={styles.mainCont}>
      <View style={styles.container}>
        <View style={styles.row}>
          <View style={styles.profile}>
            <Image
              style={{ width: "100%", height: "100%" }}
              source={require("../../../assets/profile.jpg")}
              resizeMode="cover"
            />
          </View>
          <Text style={styles.smallText}>{props?.userDetails?.name} ({props?.userDetails?.venueName})</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.smallText}>Applicant hired:- {props?.data?.data?.applicantDetails?.name}</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.smallText}>Total Amount:- {props?.data?.data?.total} GBP</Text>
        </View>
        <View style={styles.row}>
          <TouchableOpacity style={styles.viewBtn} onPress={()=>props.onPress(props?.data)}>
              <Text style={styles.btnText}>View</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainCont: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
    paddingVertical: 10,
    width: wp("100%"),
  },
  container: {
    width: "95%",
    height: hp("20%"),
    backgroundColor: "#D4A35A",
    borderRadius: 14,
    marginHorizontal: "2.5%",
    overflow: "hidden",
    padding: 10,
  },
  profile: {
    width: hp("6%"),
    height: hp("6%"),
    borderRadius: 100,
    overflow: "hidden",
  },
  row:{
      flexDirection:'row',
      alignItems:'center',
      marginVertical:2
  },
  smallText:{
    fontFamily: "MS",
    fontSize: rf(15),
    fontWeight: "700",
    color: "#fff",
    marginLeft:10
  },
  viewBtn:{
      width:'100%',
      height:hp('4%'),
      backgroundColor:'#fff',
      borderRadius:100,
      alignItems:'center',
      justifyContent:'center'
  },
  btnText:{
    fontFamily: "MS",
    fontSize: rf(15),
    fontWeight: "700",
    color: "#222",
  }
});
