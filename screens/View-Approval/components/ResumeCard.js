import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ResumeCard(props) {


  return (
    <View style={styles.container}>
      <View style={styles.firstRow}>
        <View style={styles.Pic}>
          <Image
            source={{ uri: props?.image }}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>
      </View>
      <View style={styles.SecondRow}>
        <TouchableOpacity style={styles.ResumeBtn}  onPress={props.onPress}>
          <Text style={styles.btnTxt}>{props?.image?"View resume":"No resume"}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("20%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
  },
  firstRow: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: "4%",
  },
  Pic: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 10,
    overflow: "hidden",
  },
  SecondRow: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ResumeBtn: {
    width: "80%",
    height: "60%",
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  btnTxt: {
    fontSize: rf(16),
    fontWeight: "700",
  },
});
