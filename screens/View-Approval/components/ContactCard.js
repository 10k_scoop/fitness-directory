import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { FontAwesome, Ionicons } from "@expo/vector-icons";

export default function ContactCard(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.circle} onPress={props.onPress}>
        {props.phone && <FontAwesome name="phone" size={24} color="#fff" />}
        {props.Mail && <Ionicons name="mail" size={24} color="#fff" />}
        {props.bank && <FontAwesome name="bank" size={rf(24)} color="#fff" />}
      </TouchableOpacity>
      <Text style={styles.Font}>{props.title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("20%"),
    height: hp("10%"),
    alignItems: "center",
    justifyContent: "space-between",
  },
  circle: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    alignItems: "center",
    justifyContent: "center",
  },
  Font: {
    fontSize: rf(12),
  },
});
