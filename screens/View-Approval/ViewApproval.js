import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  Linking,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import AdminBottomMenu from "../../components/AdminBottomMenu";
import ContactCard from "./components/ContactCard";
import ResumeCard from "./components/ResumeCard";
import { connect } from "react-redux";
import { ActivityIndicator } from "react-native-paper";
import {
  getUserData,
  logout,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import {
  approveUser,
  rejectUser,
} from "../../state-management/actions/Features/Actions";
import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";

const ViewApproval = (props) => {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState();
  const [showResume, setShowResume] = useState(false);
  const [status, setStatus] = useState();
  const [showBankDetails, setShowBankDetails] = useState(false);
  var data = props.route.params?.data;

  useEffect(() => {
    setStatus(props.route.params?.data?.accountStatus);
  }, []);

  const onApprove = () => {
    setLoading(true);
    props.approveUser(data.email, setLoading);
    setStatus("approved");
  };
  const onReject = () => {
    setLoading(true);
    props.rejectUser(data?.email, setLoading,props?.navigation);
    setStatus("notapproved");
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  const onCallClick = () => {
    let phoneNumber = data?.contact;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${data?.contact}`;
    } else {
      phoneNumber = `tel:${data?.contact}`;
    }
    Linking.openURL(phoneNumber);
  };

  const onShowBankDetails = () => {
    setShowBankDetails(true);
  };

  return (
    <View style={styles.container}>
      {/* Bank Details Viewer */}
      {showBankDetails && (
        <View style={styles.bankDetailsPopup}>
          <TouchableOpacity
            style={[styles.resumeCloseBtn, { left: -10, top: -10 }]}
            onPress={() => setShowBankDetails(!setShowBankDetails)}
          >
            <FontAwesome5 name="times-circle" size={30} color="#222" />
          </TouchableOpacity>
          <View style={styles.bodyHeader}>
            <FontAwesome name="bank" size={rf(30)} color="#fff" />
            <Text style={styles.headerText}>Bank details</Text>
          </View>
          <Text style={styles.bankDetailsVariablesText}>
            National Insurance Number: {"\n"}
            {data?.nationalInsuranceNumber}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Sort Code: {"\n"}
            {data?.sortCode}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Bank Name: {"\n"}
            {data?.bankName}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Account Name: {"\n"}
            {data?.accountName}
          </Text>
          <Text style={styles.bankDetailsVariablesText}>
            Account no or IBAN: {"\n"} {data?.accountNumber}
          </Text>
        </View>
      )}
      {/* Resume Viewer */}
      {showResume && (
        <View style={styles.resumeViewWrapper}>
          <TouchableOpacity
            style={styles.resumeCloseBtn}
            onPress={() => setShowResume(!showResume)}
          >
            <FontAwesome5 name="times-circle" size={24} color="white" />
          </TouchableOpacity>
          <Image
            style={{ height: "100%", width: "100%" }}
            source={{ uri: data?.applicantDocs }}
            resizeMode="contain"
          />
        </View>
      )}
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("AdminDashboard")}
      />
      <View style={styles.profileWrapper}>
        <View style={styles.profile}>
          <Image
            source={require("../../assets/ProfilePic.png")}
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>
        <View style={styles.detailView}>
          <Text style={styles.Font1}>{data?.name}</Text>
          <Text style={styles.Font2}>{data?.role}</Text>
        </View>
      </View>
      {/* Contact Cards */}
      <View style={styles.CardsWrapper}>
        <ContactCard phone title="Call Phone" onPress={() => onCallClick()} />
        <ContactCard
          Mail
          title="Send Mail"
          onPress={() =>
            Linking.openURL(
              `mailto:${data?.email}?subject=SendMail&body=Description`
            )
          }
        />
        <ContactCard
          bank
          title="Bank Details"
          onPress={() => onShowBankDetails()}
        />
      </View>
      {/* Conatct Cards */}

      <View style={styles.ResumeTitle}>
        <Text style={styles.ResumeTxt}>Resume Attachment</Text>
      </View>
      {/* Resume card */}
      <ResumeCard
        image={data?.applicantDocs}
        onPress={() =>
          data?.applicantDocs ? setShowResume(!showResume) : null
        }
      />
      <View style={styles.discription}>
        <Text style={styles.discriptionFont1}>Work Experience</Text>
        <Text style={styles.discriptionFont2}>Yoga & Gym Instructor</Text>
        <Text style={styles.discriptionFont2}>Body Building trainer</Text>
      </View>
      <View style={styles.BtnContainer}>
        <TouchableOpacity style={styles.HireBtn} onPress={() => onApprove()}>
          <Text
            style={{
              fontFamily: "MB",
              fontSize: 15,
              fontWeight: "400",
              color: "#fff",
            }}
          >
            Approve User
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rejectBtn} onPress={() => onReject()}>
          <Text
            style={{
              fontFamily: "MB",
              fontSize: 15,
              fontWeight: "400",
              color: "#fff",
            }}
          >
            Reject User
          </Text>
        </TouchableOpacity>
      </View>
      <AdminBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  profileWrapper: {
    width: wp("95%"),
    height: hp("12%"),
    borderBottomWidth: 1.5,
    borderColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
  },
  profile: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
  detailView: {
    flex: 1,
    marginLeft: "2%",
  },
  Font1: {
    fontSize: rf(14),
    fontWeight: "700",
  },
  Font2: {
    fontSize: rf(12),
    fontWeight: "700",
    color: "grey",
  },
  title: {
    width: wp("95%"),
    height: hp("7%"),
    justifyContent: "center",
  },
  CardsWrapper: {
    width: wp("100%"),
    height: hp("12%"),
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  ResumeTitle: {
    width: wp("95%"),
    height: hp("7%"),
    justifyContent: "center",
  },
  ResumeTxt: {
    fontSize: rf(16),
    fontWeight: "700",
  },
  discription: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "space-evenly",
  },
  discriptionFont1: {
    fontSize: rf(16),
    fontWeight: "700",
  },
  discriptionFont2: {
    fontSize: rf(14),
  },
  ButtonsWrapper: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  HireBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
  },
  BtnTxt: {
    fontSize: rf(18),
    fontWeight: "700",
    color: "#fff",
  },
  RejectBtn: {
    width: "25%",
    height: "50%",
    backgroundColor: "#E75D32",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  resumeViewWrapper: {
    width: wp("90%"),
    height: hp("70%"),
    position: "absolute",
    backgroundColor: "#E2B269",
    zIndex: 9999999,
    left: wp("5%"),
    top: hp("15%"),
    borderRadius: 10,
  },
  resumeCloseBtn: {
    zIndex: 99999999999999,
    position: "absolute",
  },
  bankDetailsPopup: {
    width: wp("80%"),
    minHeight: hp("40%"),
    position: "absolute",
    backgroundColor: "#E2B269",
    zIndex: 9999999999,
    left: wp("10%"),
    top: hp("35%"),
    borderRadius: 10,
    padding: hp("2%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    borderWidth: 2,
    elevation: 9,
    borderColor: "#fff",
  },
  bodyHeader: {
    marginBottom: hp("2%"),
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    fontSize: rf(20),
    fontWeight: "700",
    marginVertical: wp("2%"),
    color: "#fff",
  },
  bankDetailsVariablesText: {
    fontSize: rf(15),
    fontWeight: "700",
    marginVertical: wp("1%"),
    color: "#222",
    textAlign: "center",
  },
  BtnContainer: {
    height: hp("4%"),
    justifyContent: "center",
    paddingHorizontal: wp("10%"),
    alignItems: "center",
    marginTop:hp('12%')
  },
  HireBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#E2B269",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom:hp('2%')
  },
  rejectBtn: {
    height: hp("5%"),
    width: wp("70%"),
    backgroundColor: "#cf2129",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom:hp('2%')
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  logout,
  getUserData,
  approveUser,
  rejectUser,
})(ViewApproval);
