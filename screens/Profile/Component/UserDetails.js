import React from "react";
import { StyleSheet,Text, TouchableOpacity  } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function UserDetails({ Title }) {
    return (
        <TouchableOpacity style={styles.Body}>
           <Text style={{fontSize:rf(12), fontWeight:'900', color:'#fff'}}>{Title}</Text>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({

    Body: {
        height: hp('7%'),
        width: wp('90%'),
        backgroundColor: '#D4A35A',
        borderRadius: 100,
        justifyContent: 'center',
        paddingHorizontal: '5%',
        marginTop: '4%',


    },
  
});
