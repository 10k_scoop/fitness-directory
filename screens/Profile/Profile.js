import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Button from "../../components/Button";
import { FontAwesome, MaterialIcons } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import TextInputField from "../../components/TextInputField";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import {
  logout,
  getUserData,
} from "../../state-management/actions/auth/FirebaseAuthActions";
import { updateProfile } from "../../state-management/actions/Features/Actions";
import * as ImagePicker from "expo-image-picker";
import * as firebase from "firebase";

const Profile = (props) => {
  const [name, setName] = useState();
  const [contact, setContact] = useState();
  const [profile, setProfile] = useState();
  const [profileLoad, setProfileLoad] = useState(false);
  const [loading, setLoading] = useState(false);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    setLoading(true);
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    setLoading(true);
    if (props.get_user_details) {
      setName(props.get_user_details?.name);
      setContact(props.get_user_details?.contact);
      setProfile(props.get_user_details?.profile);
      setLoading(false);
    } else {
      setLoading(true);
    }
  }, [props]);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          //alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const onUpdate = () => {
    setLoading(true);
    let uploadUri =
      Platform.OS === "ios" ? profile.replace("file://", "") : profile;
    let data = {
      name: name,
      contact: contact,
      profile: uploadUri,
    };
    props.updateProfile(data, user.uid, setLoading);
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setProfile(result.uri);
    }
  };

  if (loading) {
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <ActivityIndicator size="large" color="orange" />
    </View>;
  }

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />
      <View style={styles.ProfilePicWrapper}>
        <View>
          <View style={styles.ProfilePicBody}>
            <Image
              style={{ height: "100%", width: "100%" }}
              source={{
                uri: profile ? profile : props.get_user_details?.profile,
              }}
              resizeMode="contain"
              onLoadStart={() => setProfileLoad(true)}
              onLoadEnd={() => setProfileLoad(false)}
            />

            {profileLoad && (
              <View
                style={{
                  position: "absolute",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <ActivityIndicator size="large" color="orange" />
              </View>
            )}
          </View>
          <TouchableOpacity
            style={styles.UploadPic}
            onPress={() => pickImage()}
          >
            <MaterialIcons name="photo-camera" size={24} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.TextFieldWrapper}>
        <TextInputField
          onChangeText={(val) => setName(val)}
          placeholder={props.get_user_details?.name}
        />
        <TextInputField email placeholder={props.get_user_details?.email} />
        <TextInputField
          onChangeText={(val) => setContact(val)}
          placeholder={props.get_user_details?.contact}
        />
      </View>
      <View style={styles.BtnWrapper}>
        <Button
          BtnText={
            loading ? <ActivityIndicator size="small" color="#fff" /> : "Update"
          }
          onPress={() => onUpdate()}
        />
      </View>
      <View style={styles.SettingWrapper}>
        <TouchableOpacity
          style={styles.SetingBody}
          onPress={() => props.navigation.navigate("InstructorSettings")}
        >
          <Text
            style={{
              fontFamily: "MM",
              fontSize: rf(13),
              fontWeight: "500",
              color: "#fff",
            }}
          >
            Settings
          </Text>
          <View
            style={{
              height: "70%",
              width: "20%",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              style={{ height: "80%", width: "80%" }}
              source={require("../../assets/SettingIcon.png")}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity>
      </View>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#e5e5e5",
  },
  ProfilePicWrapper: {
    height: hp("25%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "center",
  },
  ProfilePicBody: {
    height: hp("16%"),
    width: hp("16%"),
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    backgroundColor: "#fff",
  },
  UploadPic: {
    height: hp("5%"),
    width: hp("5%"),
    borderRadius: 100,
    backgroundColor: "#D4A35A",
    bottom: 0,
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  TextFieldWrapper: {
    height: hp("30%"),
    width: wp("100%"),
    alignItems: "center",
  },
  BtnWrapper: {
    height: hp("12%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "center",
  },
  SettingWrapper: {
    height: hp("10%"),
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "center",
  },
  SetingBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginTop: "4%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, updateProfile, getUserData })(
  Profile
);
