import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Switch,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import BottomMenu from "../../components/BottomMenu";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { changePassword } from "../../state-management/actions/auth/FirebaseAuthActions";
import VenueBottomMenu from "../../components/VenueBottomMenu";

const ChangePassword = (props) => {
  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [confirmNewPass, setConfirmNewPass] = useState("");
  const [loading, setLoading] = useState(false);
  const onSubmit = () => {
    if (newPass != "" && confirmNewPass != "") {
      if (newPass == confirmNewPass) {
        let data = {
          oldPass: oldPass,
          newPass: newPass,
          email: props.get_user_details?.email,
        };
        setLoading(true);
        props.changePassword(data, setLoading);
        setOldPass("");
        setNewPass("");
        setConfirmNewPass("");
      } else {
        alert("Password doest not match");
      }
    } else {
      alert("fill all fields");
    }
  };

  return (
    <View style={styles.container}>
      <Header
        onHomeClick={() => props.navigation.navigate("InstructorDashboard")}
        onLogoutPress={() => props.logout()}
      />
      <View
        style={{
          alignItems: "center",
          flex: 1,
          marginTop: hp("5%"),
        }}
      >
        <View style={styles.textField}>
          <TextInput
            style={{
              width: "100%",
              height: "90%",
              paddingHorizontal: 10,
              fontFamily: "MR",
            }}
            secureTextEntry={true}
            placeholder="Old Password"
            color="#222"
            placeholderTextColor="#222"
            onChangeText={(val) => setOldPass(val)}
          />
        </View>
        <View style={styles.textField}>
          <TextInput
            style={{
              width: "100%",
              height: "90%",
              paddingHorizontal: 10,
              fontFamily: "MR",
            }}
            secureTextEntry={true}
            placeholder="New Password"
            color="#222"
            placeholderTextColor="#222"
            onChangeText={(val) => setNewPass(val)}
          />
        </View>
        <View style={styles.textField}>
          <TextInput
            style={{
              width: "100%",
              height: "90%",
              paddingHorizontal: 10,
              fontFamily: "MR",
            }}
            placeholder="Confrim New Password"
            color="#222"
            secureTextEntry={true}
            placeholderTextColor="#222"
            onChangeText={(val) => setConfirmNewPass(val)}
          />
        </View>
        <TouchableOpacity style={styles.submitBtn} onPress={onSubmit}>
          {loading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <Text
              style={{
                fontFamily: "MS",
                fontSize: rf(14),
                fontWeight: "500",
                color: "#fff",
              }}
            >
              Submit
            </Text>
          )}
        </TouchableOpacity>
      </View>
      {props.route.params?.role == "instructor" ? (
        <BottomMenu navigation={props.navigation} />
      ) : (
        <VenueBottomMenu navigation={props.navigation} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  submitBtn: {
    height: hp("5%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  textField: {
    height: hp("5%"),
    width: wp("90%"),
    backgroundColor: "transparent",
    borderRadius: 10,
    marginHorizontal: "2.5%",
    marginTop: 10,
    paddingHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#D4A35A",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout, changePassword })(
  ChangePassword
);
