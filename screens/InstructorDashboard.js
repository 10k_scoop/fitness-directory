import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TextInput,
  RefreshControl,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import MenuBtn from "../components/MenuBtn";
import GymCards from "../components/GymCards";
import BottomMenu from "../components/BottomMenu";
import {
  logout,
  getUserData,
} from "../state-management/actions/auth/FirebaseAuthActions";
import {
  addJobToFavourites,
  getJobs,
  updateFavourite,
  searchJob,
} from "../state-management/actions/Features/Actions";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const InstructorDashboard = (props) => {
  const [loading, setLoading] = useState(false);
  const [jobs, setJobs] = useState([]);
  const [searchText, setSearchText] = useState("");
  const user = firebase.auth().currentUser;

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.getUserData(user.uid, setLoading);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    console.log("here instrruxctoe")
    setLoading(true);
    props.getJobs(setLoading);
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    setJobs(props?.get_jobs);
  }, [props]);
 
  const addToFavourites = (fav, id) => {
    if (fav.includes(user.email)) {
      fav.pop(user.email);
      let data = {
        favourites: fav,
        id: id,
      };
      props.updateFavourite(data);
    } else {
      fav.push(user.email);
      let data = {
        favourites: fav,
        id: id,
      };
      props.updateFavourite(data);
    }
  };

  const onSearch = (val) => {
    setSearchText("");
    setLoading(true);
    props.searchJob(val.toLowerCase(), setLoading);
  };

  return (
    <View style={styles.container}>
      <Header onLogoutPress={() => props?.logout()} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.Main}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: hp("2%"),
              width: "95%",
            }}
          >
            <TextInput
              placeholder="Search here..."
              onChangeText={(val) => setSearchText(val)}
              style={styles.SearchBar}
              value={searchText}
            />
            <TouchableOpacity
              style={styles.searchIcon}
              onPress={() => onSearch(searchText)}
            >
              <AntDesign name="search1" size={rf(18)} color="white" />
            </TouchableOpacity>
          </View>
          <View style={styles.Menu}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <MenuBtn
                txt="All"
                onPress={() => {
                  setLoading(true);
                  props.getJobs(setLoading);
                }}
              />
              <MenuBtn
                txt="Personal Trainer"
                onPress={() => onSearch("Personal Trainer")}
              />
              <MenuBtn txt="Zumbakara" onPress={() => onSearch("Zumbakara")} />
              <MenuBtn txt="Yoga" onPress={() => onSearch("Yoga")} />
            </ScrollView>
          </View>

          {/* Card */}
          <View style={styles.cardsWrapper}>
            <Text
              style={{
                fontFamily: "MB",
                fontSize: rf(22),
                left: 10,
                marginBottom: 10,
              }}
            >
              Related Jobs
            </Text>
            {jobs?.length < 1 && !loading && <Text>No related job found</Text>}
            {loading ? (
              <ActivityIndicator size="large" color="orange" />
            ) : (
              jobs?.map((item, index) => {
                return (
                  <GymCards
                    key={index}
                    status={
                      item.applicants.includes(user.email) ? "applied" : "apply"
                    }
                    data={item}
                    onApplyClick={() =>
                      props.navigation.navigate("ViewJob", { data: item })
                    }
                    onHeartPress={() =>
                      addToFavourites(item.favourites, item.id)
                    }
                    favourite={item.favourites.includes(user.email)}
                  />
                );
              })
            )}
          </View>
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "center",
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  cardsWrapper: {
    marginBottom: hp("4%"),
  },
  SearchBar: {
    width: wp("85%"),
    height: hp("5%"),
    borderWidth: 0.3,
    borderColor: "#e5e5e5",
    borderRadius: 100,
    borderColor: "#aca7a6",
    paddingHorizontal: "5%",
    fontSize: rf(11),
    fontFamily: "MR",
  },
  searchIcon: {
    alignItems: "center",
    justifyContent: "center",
    left: 10,
    backgroundColor: "orange",
    width: wp("8%"),
    height: wp("8%"),
    borderRadius: 100,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
  get_jobs: state.main.get_jobs,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  logout,
  addJobToFavourites,
  getJobs,
  getUserData,
  updateFavourite,
  searchJob,
})(InstructorDashboard);
