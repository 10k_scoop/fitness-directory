import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  RefreshControl,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../components/Header";
import GymCards from "../components/GymCards";
import { connect } from "react-redux";
import { myJobs } from "../state-management/actions/Features/Actions";
import VenueBottomMenu from "../components/VenueBottomMenu";
import * as firebase from "firebase";
import { logout } from "../state-management/actions/auth/FirebaseAuthActions";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const Myjobs = (props) => {
  const [loading, setLoading] = useState(false);
  const [myJobs, setMyJobs] = useState([]);
  const user = firebase.auth().currentUser;

  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    props.myJobs(user.email, setLoading);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.myJobs(user.email, setLoading);
  }, []);

  useEffect(() => {
    setMyJobs(props?.my_jobs);
  }, [props]);

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.Main}>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "700", left: 5 }}>
              My Jobs
            </Text>
          </View>

          {myJobs?.length < 1 && !loading && <Text>No related job found</Text>}
          {loading ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            myJobs?.map((item, index) => {
              return (
                <GymCards
                  onApplyClick={() =>
                    props.navigation.navigate("VenueJob", { data: item })
                  }
                  data={item}
                  key={index}
                  View
                  noHeart
                />
              );
            })
          )}
        </View>
      </ScrollView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingLeft: 10,
  },
  Main: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: "10%",
  },
  title: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  my_jobs: state.main.my_jobs,
});
export default connect(mapStateToProps, { myJobs, logout })(Myjobs);
