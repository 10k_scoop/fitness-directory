import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TextInput } from "react-native-gesture-handler";

export default function Skils(props) {
  return (
    <View>
      <View style={styles.SkilCategoryBody}>
        <Text
          style={{
            fontFamily: "MS",
            fontWeight: "400",
            fontSize: rf(11),
            color: "#fff",
          }}
        >
          {props.Skill}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.remove}
        onPress={() => props.onRemove(props.id)}
      >
        <Text style={{ fontFamily: "MS", color: "white", fontSize: 12 }}>
          X
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  SkilCategoryBody: {
    height: hp("4%"),
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    marginBottom: hp("2%"),
    paddingHorizontal: wp("2%"),
    minWidth: wp("15%"),
    marginHorizontal: 10,
  },
  remove: {
    width: 15,
    height: 15,
    backgroundColor: "#E75D32",
    borderRadius: 100,
    position: "absolute",
    left: 5,
    top: -5,
    opacity: 0.8,
    alignItems: "center",
    justifyContent: "center",
  },
});
