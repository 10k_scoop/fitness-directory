import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TextInput } from "react-native-gesture-handler";

export default function PostJobTextField(props) {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.FieldStyles}
        placeholder={props.placeholder}
        placeholderTextColor="#fff"
        onChangeText={props.onChangeText}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    justifyContent: "center",
    marginBottom: 20,
  },
  FieldStyles: {
    fontSize: rf(11),
    fontWeight: "400",
    height: "100%",
    width: "100%",
    color:"#fff"
  },
});
