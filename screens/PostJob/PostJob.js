import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "../../components/Header";
import Skils from "./components/Skills";
import JobPicker from "./components/JobPicker";
import PostJobTextField from "./components/PostJobTextField";
import { TextInput } from "react-native-gesture-handler";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import { postJob } from "../../state-management/actions/Features/Actions";
import { connect } from "react-redux";
import * as firebase from "firebase";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";
import { Picker } from "@react-native-picker/picker";
const PostJob = (props) => {
  const [jobTitle, setJobTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [location, setLocation] = useState("");
  const [totalPositions, setTotalPositions] = useState("");
  const [jobType, setJobType] = useState("");
  const [jobShift, setJobShift] = useState("");
  const [gender, setGender] = useState("");
  const [experience, setExperience] = useState("");
  const [skillText, setSkillText] = useState("");
  const [salary, setSalary] = useState("");
  const [loading, setLoading] = useState(false);
  const [skills, setSkills] = useState([]);
  const [skillList, setSkillList] = useState("");

  const onAddSkill = (fromSkillList) => {
    if (skillText != "") {
      setSkills((prevstate) => [...prevstate, skillText]);
      setSkillText("");
    }
  };
  const onRemoveSkill = (value) => {
    let filteredArray = skills.filter((item) => item !== value);
    setSkills(filteredArray);
  };
  const user = firebase.auth().currentUser;
  const onSubmit = () => {
    let totalSkill = skills;
    totalSkill.push(skillList);
    let data = {
      title: jobTitle,
      describtion: desc,
      hiredSomeone:false,
      location: location,
      skills: totalSkill,
      totalPositions: totalPositions,
      jobType: jobType,
      jobShift: jobShift,
      gender: gender,
      experience: experience,
      salary: salary,
      applicants: [],
      price:salary=="Fixed classes £32"?32:15,
      favourites: [],
      date: new Date(),
      thumbnail: "",
      author: user.email,
    };

    if (
      jobTitle == "" ||
      desc == "" ||
      location == "" ||
      totalPositions == "" ||
      jobType == "" ||
      skillList == ""
    ) {
      alert("Fill all details");
      setSkillList("");
    } else {
      setLoading(true);
      props.postJob(data, setLoading);
      setSkills([]);
      setDesc("");
      setJobShift("");
      setJobTitle("");
      setJobType("");
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        onLogoutPress={() => props.logout()}
        onHomeClick={() => props.navigation.navigate("VenueDashboard")}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <View style={styles.FieldWrapper}>
            <View>
              <PostJobTextField
                placeholder="Job Title"
                onChangeText={(val) => setJobTitle(val)}
              />
            </View>
            <View style={styles.JobDescriptionBody}>
              <TextInput
                style={styles.DescriptionTxt}
                placeholder="Lorem Ipsum"
                multiline={true}
                placeholderTextColor="#fff"
                onChangeText={(val) => setDesc(val)}
              />
            </View>
            <View>
              <PostJobTextField
                placeholder="Location"
                onChangeText={(val) => setLocation(val)}
              />
            </View>
            <View style={styles.picker}>
              <Text
                style={{ fontWeight: "400", fontSize: rf(11), color: "#fff" }}
              >
                Skills
              </Text>
              <View style={{ flex: 1 }}>
                <Picker
                  selectedValue={skillList}
                  onValueChange={(itemValue, itemIndex) =>
                    setSkillList(itemValue)
                  }
                  itemStyle={{ color: "#fff", fontSize: rf(13) }}
                >
                  <Picker.Item label="Select a Skill" value={null} />
                  <Picker.Item label="Gym instructor" value="Gym Instructor" />
                  <Picker.Item
                    label="Personal trainer"
                    value="Personal trainer"
                  />
                  <Picker.Item label="Body pump" value="Body pump" />
                  <Picker.Item label="Body attack" value="Body attack" />
                  <Picker.Item label="Body combat" value="Body combat" />
                  <Picker.Item label="Body balance" value="Body balance" />
                  <Picker.Item label="Yoga" value="Yoga" />
                  <Picker.Item label="Pilates" value="Pilates" />
                  <Picker.Item label="Fightklub" value="Fightklub" />
                  <Picker.Item label="Zumba" value="Zumba" />
                  <Picker.Item label="Aqua aerobics" value="Aqua aerobics" />
                  <Picker.Item label="LBT" value="LBT" />
                  <Picker.Item label="Lifeguard" value="Lifeguard" />
                  <Picker.Item label="Sports massage" value="Sports massage" />
                  <Picker.Item
                    label="Strength and conditioning"
                    value="Strength and conditioning"
                  />
                </Picker>
              </View>
            </View>
            <View style={styles.SkillsBody}>
              <TextInput
                placeholderTextColor="#fff"
                style={styles.SkillsTxt}
                value={skillText}
                placeholder="Add custom Skill (optional)"
                onChangeText={(val) => setSkillText(val.toLowerCase())}
              />
              <TouchableOpacity onPress={onAddSkill}>
                <AntDesign name="pluscircleo" size={24} color="#fff" />
              </TouchableOpacity>
            </View>
            <View style={styles.SkilCategory}>
              {skills.map((item, index) => {
                return (
                  <Skils
                    Skill={item}
                    key={index}
                    onRemove={() => onRemoveSkill(item)}
                  />
                );
              })}
            </View>
            <View>
              <PostJobTextField
                placeholder="Total Positions"
                onChangeText={(val) => setTotalPositions(val)}
              />
            </View>

            <View style={styles.PickerWrapper}>
              <JobPicker
                Title="Job Type"
                hookType={jobType}
                onValueChange={(val) => setJobType(val)}
                items={[
                  { label: "Part Time", value: "Part Time" },
                  { label: "Full Time", value: "Full Time" },
                ]}
              />
              <JobPicker
                Title="Job Shift"
                hookType={jobShift}
                onValueChange={(val) => setJobShift(val)}
                items={[
                  { label: "Morning", value: "Morning" },
                  { label: "Evening", value: "Evening" },
                ]}
              />
              <JobPicker
                Title="Gender"
                hookType={gender}
                onValueChange={(val) => setGender(val)}
                items={[
                  { label: "Male", value: "Male" },
                  { label: "Female", value: "Female" },
                ]}
              />
              <JobPicker
                Title="Experience"
                hookType={experience}
                onValueChange={(val) => setExperience(val)}
                items={[
                  { label: "1-2", value: "1-2" },
                  { label: "3-5", value: "3-5" },
                  { label: "5+", value: "5+" },
                ]}
              />
              <JobPicker
                Title="Salary"
                hookType={salary}
                onValueChange={(val) => setSalary(val)}
                items={[
                  { label: "Fixed classes £32", value: "Fixed classes £32" },
                  { label: "Fixed hourly £15", value: "Fixed hourly £15" },
                ]}
              />
            </View>
            <View style={styles.SubmitBtnWrapper}>
              <TouchableOpacity style={styles.SubBtn} onPress={onSubmit}>
                <Text
                  style={{ fontSize: rf(11), fontWeight: "400", color: "#fff" }}
                >
                  Submit Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <VenueBottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  FieldWrapper: {
    width: wp("100%"),
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    marginTop: 10,
    marginBottom: hp("15%"),
  },
  JobDescriptionBody: {
    height: hp("20%"),
    width: "100%",
    backgroundColor: "#D4A35A",
    borderRadius: 20,
    padding: "5%",
    marginBottom: 20,
  },
  DescriptionTxt: {
    fontSize: rf(11),
    fontWeight: "400",
    height: "100%",
    width: "100%",
    color: "#fff",
    textAlignVertical: "top",
  },
  SkillsBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginBottom: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  SkillsTxt: {
    fontSize: rf(11),
    fontWeight: "400",
    height: "100%",
    width: "90%",
    color: "#fff",
  },
  SkilCategory: {
    flexWrap: "wrap",
    flexDirection: "row",
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  PickerWrapper: {
    alignItems: "center",
  },
  SubmitBtnWrapper: {
    height: hp("15%"),
    alignItems: "center",
    justifyContent: "center",
    bottom: hp("5%"),
  },
  SubBtn: {
    height: hp("5%"),
    width: wp("80%"),
    borderRadius: 100,
    backgroundColor: "#E2B269",
    justifyContent: "center",
    alignItems: "center",
  },
  picker: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#E2B269",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 100,
    marginBottom: 20,
    paddingHorizontal: 20,
    overflow: "hidden",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  post_job: state.main.post_job,
  
});
export default connect(mapStateToProps, { postJob, logout })(PostJob);
