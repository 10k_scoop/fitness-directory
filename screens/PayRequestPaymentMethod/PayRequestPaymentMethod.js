import React, { useState } from "react";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
} from "react-native";
import { CardField, useConfirmPayment } from "@stripe/stripe-react-native";
import { StripeProvider } from "@stripe/stripe-react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { addRequestPaymentDetailsToFirebase } from "../../state-management/actions/Features/Actions";
import { ActivityIndicator } from "react-native-paper";
import Header from "../../components/Header";
import VenueBottomMenu from "../../components/VenueBottomMenu";
import { TouchableOpacity } from "react-native-gesture-handler";

//ADD localhost address of your server
// const API_URL = "https://fitness-directory.herokuapp.com";
const API_URL = "https://fitness-directory.herokuapp.com";

const PayRequestPaymentMethod = (props) => {
  const [cardDetails, setCardDetails] = useState();
  const [loader, setLoader] = useState(false);
  const [paymentDone, setPaymentDone] = useState(false);
  const { confirmPayment, loading } = useConfirmPayment();

  let prevData = props.route.params?.data;
  let removalData = props.route.params?.removalData;
  
  const fetchPaymentIntentClientSecret = async () => {
    const response = await fetch(`${API_URL}/create-payment-intent`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ data: prevData }),
    });
    const { clientSecret, error } = await response.json();
    return { clientSecret, error };
  };

  const handlePayPress = async () => {
    //1.Gather the customer's billing information (e.g., email)
    if (!cardDetails?.complete) {
      Alert.alert("Please enter Complete card details and Email");
      return;
    }
    const billingDetails = {
      email: prevData?.recruiterEmail,
    };
    //2.Fetch the intent client secret from the backend
    try {
      setLoader(true);
      const { clientSecret, error } = await fetchPaymentIntentClientSecret();
      //2. confirm the payment
      if (error) {
        setLoader(false);
        console.log("Unable to process payment");
      } else {
        const { paymentIntent, error } = await confirmPayment(clientSecret, {
          type: "Card",
          billingDetails: billingDetails,
        });
        if (error) {
          setLoader(false);
          alert(`Payment Confirmation Error ${error.message}`);
        } else if (paymentIntent) {
          console.log("Payment successful ", paymentIntent);
          let res = {
            data: prevData,
            paymentIntent: paymentIntent,
          };
          await props.addRequestPaymentDetailsToFirebase(
            res,
            setLoader,
            removalData
          );
          alert("Payment Successful");
          setTimeout(function () {
            props.navigation.navigate("VenueDashboard");
          }, 2000);
        }
      }
    } catch (e) {
      setLoader(false);
      console.log(e);
    }
    //3.Confirm the payment with the card details
  };

  return (
    <StripeProvider publishableKey="pk_test_51GubA8LrQYi8DtCUhSYrFSWKhbzQ2q0wO5A1Q1izE92vVhmMPQa5eOiOmWO5si2mS3nj7yHmubMfHq2Q4T3xTsQD003fPTZqMp">
      <View style={styles.container}>
        <ScrollView>
          {loader && (
            <View style={styles.loader}>
              <ActivityIndicator size="large" color="black" />
            </View>
          )}
          <Header
            onLogoutPress={() => props.logout()}
            onHomeClick={() => props.navigation.navigate("VenueDashboard")}
          />
          <View style={styles.bodyWrapper}>
            <Text style={styles.title}>Order details</Text>

            <View style={styles.orderDetailsWrapper}>
              <Text style={styles.deatilsTitle}>Instructor Details:-</Text>
              <Text style={styles.deatilsSubText}>
                Name:- {prevData?.applicantDetails?.name}
              </Text>
              <Text style={styles.deatilsSubText}>
                Contact:- {prevData?.applicantDetails?.contact}
              </Text>
              <Text style={styles.deatilsTitle}>Job Details:-</Text>
              <Text style={styles.deatilsSubText}>
                Title :- {prevData?.jobDetails?.title}
              </Text>
              <Text style={styles.deatilsSubText}>
                Job Shift :- {prevData?.jobDetails?.jobShift}
              </Text>
              <Text style={styles.deatilsSubText}>
                Cost:- {prevData?.jobDetails?.salary}
              </Text>
              <Text style={styles.deatilsSubText}>
                Total Hours :- {prevData?.hours} x 6 days ={" "}
                {prevData?.hours * 6} hrs
              </Text>
              <Text style={styles.deatilsSubText}>
                Total Cost :- {"£ " + prevData?.total}
              </Text>
              <Text style={styles.deatilsTitle}>Your Details:-</Text>
              <Text style={styles.deatilsSubText}>
                Email :- {prevData?.recruiterEmail}
              </Text>
            </View>
            <Text style={styles.title}>Enter Card details</Text>
            <CardField
              postalCodeEnabled={true}
              placeholder={{
                number: "4242 4242 4242 4242",
              }}
              cardStyle={styles.card}
              style={styles.cardContainer}
              onCardChange={(cardDetails) => {
                setCardDetails(cardDetails);
              }}
            />
            <TouchableOpacity
              style={styles.addPaymentMethodButton}
              onPress={handlePayPress}
              disabled={loading}
            >
              <Text style={styles.addPaymentMethodButtonText}>Pay now</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <VenueBottomMenu navigation={props.navigation} />
      </View>
    </StripeProvider>
  );
};
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  view_job: state.main.view_job,
});
export default connect(mapStateToProps, { addRequestPaymentDetailsToFirebase })(
  PayRequestPaymentMethod
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bodyWrapper: {
    padding: 10,
    flex: 1,
    width: "100%",
  },
  orderDetailsWrapper: {
    width: "100%",
    minHeight: hp("15%"),
    padding: 10,
    marginTop: hp("0%"),
    borderBottomWidth: 1,
    borderColor: "#cdcdcd",
  },
  orderDetailsProfile: {
    width: hp("8%"),
    height: hp("8%"),
    backgroundColor: "red",
    borderRadius: 100,
    overflow: "hidden",
  },
  title: {
    fontSize: rf(18),
    marginLeft: 10,
    fontFamily: "MB",
    color: "#A59F9F",
    marginVertical: 5,
  },
  deatilsTitle: {
    fontSize: rf(16),
    color: "#222",
    fontFamily: "MB",
    marginBottom: 5,
  },
  deatilsSubText: {
    fontSize: rf(12),
    color: "#222",
    fontFamily: "MM",
    marginVertical: 5,
  },
  addPaymentMethodButton: {
    width: wp("90%"),
    height: hp("5%"),
    backgroundColor: "#F5CA48",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("3%"),
    borderRadius: 5,
    paddingHorizontal: wp("5%"),
    alignSelf: "center",
  },
  addPaymentMethodButtonText: {
    fontFamily: "MB",
    fontSize: rf(18),
    color: "#fff",
    marginLeft: 10,
  },
  cardContainer: {
    height: 50,
    marginVertical: 10,
  },
  card: {
    backgroundColor: "#efefef",
    borderRadius: 10,
  },
  loader: {
    position: "absolute",
    zIndex: 99999999,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    opacity: 0.5,
  },
});
