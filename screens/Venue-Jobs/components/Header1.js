import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { Entypo, AntDesign } from '@expo/vector-icons';

export default function Header1(props) {
    return (
        <View style={styles.container}>
            <View style={styles.backIcon}>
                <TouchableOpacity onPress={props.onBackClick}>
                    <AntDesign name="arrowleft" size={rf(20)} color="black" />
                </TouchableOpacity>
                <Text style={{ fontSize: rf(14), marginLeft: 10 }}>Back to jobs</Text>
            </View>
            <Text style={{fontSize:rf(18),fontWeight:"700"}}>{props.title}</Text>
            <Text style={{fontSize:rf(15)}}>{props.location}</Text>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp('95%'),
        height: hp('14%'),
        paddingHorizontal:"3%"

    },
    backIcon: {
        width: "100%",
        height: "40%",
        flexDirection: "row",
        alignItems: "center",
    }

});
