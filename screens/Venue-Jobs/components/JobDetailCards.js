import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";


export default function JobDetailCards({ txt,txt1 }) {
    return (
        <View style={styles.container}>
            <Text style={{fontSize:rf(14),fontWeight:"700"}}>{txt}</Text>
            <Text style={{fontSize:rf(14)}}>{txt1}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp('90%'),
        height: hp('3.5%'),
        flexDirection:"row",
        justifyContent:"space-between",
        paddingHorizontal:"2%",
        alignItems:"center"


    },


});
