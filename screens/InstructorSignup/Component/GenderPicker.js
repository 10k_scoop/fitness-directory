import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
export default function GenderPicker(props) {
  return (
    <View style={styles.GenderBody}>
      <Text style={{ fontWeight: "500", fontSize: rf(12), color: "#fff" }}>
        Gender
      </Text>
      <View style={styles.PickerBody}>
        <TouchableOpacity
          style={[
            styles.GenderPicker,
            { backgroundColor: props.gender == "male" ? "#D4A35A" : "#fff" },
          ]}
          onPress={props.onMalePress}
        ></TouchableOpacity>
        <Text
          style={{
            fontWeight: "500",
            fontSize: rf(12),
            marginLeft: 10,
            color: "#fff",
          }}
        >
          Male
        </Text>
      </View>
      <View style={[styles.PickerBody, { right: wp("5%") }]}>
        <TouchableOpacity
          style={[
            styles.GenderPicker,
            { backgroundColor: props.gender == "female" ? "#D4A35A" : "#fff" },
          ]}
          onPress={props.onFeMalePress}
        ></TouchableOpacity>
        <Text
          style={{
            fontWeight: "500",
            fontSize: rf(12),
            marginLeft: 10,
            color: "#fff",
          }}
        >
          Female
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  GenderBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  GenderPicker: {
    height: wp("3%"),
    width: wp("3%"),
    borderRadius: 100,
    borderWidth: 3,
    borderColor: "#fff",
  },
  PickerBody: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
});
