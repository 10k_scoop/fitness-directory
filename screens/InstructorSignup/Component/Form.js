import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import TextInputField from "../../../components/TextInputField";
import GenderPicker from "./GenderPicker";
import { AntDesign, Feather } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Button from "../../../components/Button";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Picker } from "@react-native-picker/picker";

const Form = (props) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [address, setAddress] = useState("");
  const [gender, setGender] = useState("male");
  const [dateOfBirth, setDateOfBirth] = useState(new Date(1598051730000));
  const [contact, setContact] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [qualification, setQualification] = useState("");
  const [isTerms, setIsTerms] = useState("");
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const [nationalInsuranceNumber, setNationalInsuranceNumber] = useState("");
  const [sortCode, setSortCode] = useState("");
  const [bankName, setBankName] = useState("");
  const [accountName, setAccountName] = useState("");
  const [accountNumber, setAccountNumber] = useState("");

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || dateOfBirth;
    setShow(Platform.OS === "ios");
    setDateOfBirth(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const onSubmit = () => {
    let data = {
      email: email.toLowerCase(),
      password: pass,
      name: name,
      gender: gender,
      contact: contact,
      dateOfBirth: dateOfBirth,
      qualification: qualification,
      role: "instructor",
      accountStatus: "notapproved",
      skills: [],
      profile:
        "https://www.kindpng.com/picc/m/78-785827_user-profile-avatar-login-account-male-user-icon.png",
      documents: [],
      hired: false,
      hiredById: null,
      hiredByEmail: null,
      bankName: bankName,
      accountName: accountName,
      accountNumber: accountNumber,
      nationalInsuranceNumber:nationalInsuranceNumber,
      sortCode:sortCode,
      address:address,
    };

    if (
      email != "" &&
      pass != "" &&
      confirmPassword != "" &&
      isTerms &&
      name != "" &&
      bankName != "" &&
      accountName != "" &&
      accountNumber != "" &&
      address != "" &&
      sortCode != "" &&
      nationalInsuranceNumber != ""
    ) {
      if (pass == confirmPassword) {
        props.onSubmit(data);
      } else {
        alert("Password doesn't match");
      }
    } else {
      alert("Fill all details");
    }
  };

  return (
    <View style={styles.NameWrapper}>
      <TextInputField
        placeholder="Name"
        type="default"
        onChangeText={(val) => setName(val)}
      />
      <TextInputField
        placeholder="Email"
        type="email-address"
        onChangeText={(val) => setEmail(val)}
      />
      <GenderPicker
        gender={gender}
        onMalePress={() => setGender("male")}
        onFeMalePress={() => setGender("female")}
      />
      <View style={styles.ContactBody}>
        <View style={{ width: "60%" }}>
          {show ? (
            <DateTimePicker
              testID="dateTimePicker"
              value={dateOfBirth}
              mode={mode}
              display="calendar"
              onChange={onChange}
            />
          ) : (
            <Text style={{ color: "#fff" }}>
              {" "}
              {dateOfBirth.getDate() +
                "-" +
                dateOfBirth.getMonth() +
                "-" +
                dateOfBirth.getFullYear() || "Date of Birth"}
            </Text>
          )}
        </View>
        <TouchableOpacity onPress={showDatepicker}>
          <AntDesign name="calendar" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
      <TextInputField
        placeholder="Contact"
        type="number-pad"
        onChangeText={(val) => setContact(val)}
      />
       <TextInputField
        placeholder="Address"
        type="text"
        onChangeText={(val) => setAddress(val)}
      />
      <TextInputField
        placeholder="Password"
        type="default"
        secureTextEntry
        onChangeText={(val) => setPass(val)}
      />
      <TextInputField
        placeholder="Confirm Password"
        type="default"
        secureTextEntry
        onChangeText={(val) => setConfirmPassword(val)}
      />

      <View style={styles.picker}>
        <Picker
          selectedValue={qualification}
          onValueChange={(itemValue, itemIndex) => setQualification(itemValue)}
          itemStyle={{ color: "#fff", fontSize: rf(13) }}
          style={{ color: "#fff" }}
        >
          <Picker.Item label="select qualification" value={null} />
          <Picker.Item label="Gym instructor" value="Gym Instructor" />
          <Picker.Item label="Personal trainer" value="Personal trainer" />
          <Picker.Item label="Body pump" value="Body pump" />
          <Picker.Item label="Body attack" value="Body attack" />
          <Picker.Item label="Body combat" value="Body combat" />
          <Picker.Item label="Body balance" value="Body balance" />
          <Picker.Item label="Yoga" value="Yoga" />
          <Picker.Item label="Pilates" value="Pilates" />
          <Picker.Item label="Fightklub" value="Fightklub" />
          <Picker.Item label="Zumba" value="Zumba" />
          <Picker.Item label="Aqua aerobics" value="Aqua aerobics" />
          <Picker.Item label="LBT" value="LBT" />
          <Picker.Item label="Lifeguard" value="Lifeguard" />
          <Picker.Item label="Sports massage" value="Sports massage" />
          <Picker.Item
            label="Strength and conditioning"
            value="Strength and conditioning"
          />
        </Picker>
      </View>
      <Text
        style={{
          fontSize: rf(16),
          color: "#fff",
          marginVertical: 10,
          fontWeight: "bold",
        }}
      >
        Add Bank Details
      </Text>
      <TextInputField
        placeholder="National insurance number"
        type="default"
        onChangeText={(val) => setNationalInsuranceNumber(val)}
      />
      <TextInputField
        placeholder="Bank Name"
        type="default"
        onChangeText={(val) => setBankName(val)}
      />
      <TextInputField
        placeholder="Account Name"
        type="default"
        onChangeText={(val) => setAccountName(val)}
      />
      <TextInputField
        placeholder="Account Number or IBAN"
        type="number-pad"
        onChangeText={(val) => setAccountNumber(val)}
      />
        <TextInputField
        placeholder="Sort Code"
        type="text"
        onChangeText={(val) => setSortCode(val)}
      />
      {/* Terms and conditions */}
      <View style={styles.TermsBody}>
        <TouchableOpacity onPress={() => setIsTerms(!isTerms)}>
          {isTerms ? (
            <AntDesign name="checksquare" size={rf(15)} color="#fff" />
          ) : (
            <Feather name="square" size={rf(15)} color="#fff" />
          )}
        </TouchableOpacity>
        <Text style={styles.termsText}>I agree to the Terms & Conditions</Text>
      </View>
      <View style={styles.BtnView}>
        <Button BtnText="Signup" onPress={onSubmit} />
      </View>
      <View style={styles.TxtLogin}>
        <Text style={styles.Text}>Already have an account? </Text>
        <TouchableOpacity onPress={props.onLoginPress}>
          <Text style={styles.Text}> Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  NameWrapper: {
    marginBottom: 20,
    width: wp("100%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
  ContactBody: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    paddingHorizontal: "5%",
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Pickerr: {
    flex: 1,
  },
  TermsTextWrapper: {
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
  },
  TermsBody: {
    width: "100%",
    paddingHorizontal: wp("10%"),
    flexDirection: "row",
    alignItems: "center",
    marginVertical: hp("2%"),
  },

  BtnView: {
    height: hp("10%"),
    justifyContent: "center",
    alignItems: "center",
  },
  TxtLogin: {
    width: wp("100%"),
    justifyContent: "center",
    flexDirection: "row",
  },
  termsText: {
    fontWeight: "600",
    fontSize: rf(11),
    color: "#fff",
    marginLeft: "5%",
  },

  Text: {
    fontWeight: "600",
    fontSize: rf(12),
    color: "#fff",
  },
  datePicker: {
    position: "absolute",
    width: "100%",
    height: "100%",
    zIndex: 9999999,
  },
  picker: {
    height: hp("6%"),
    width: wp("90%"),
    backgroundColor: "#D4A35A",
    borderRadius: 100,
    justifyContent: "center",
    paddingHorizontal: "2%",
    marginTop: 10,
    overflow: "hidden",
  },
});

export default Form;
