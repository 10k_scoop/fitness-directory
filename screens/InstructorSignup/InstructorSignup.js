import React, { useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Image,
  ScrollView,
  StatusBar,
  Platform,
  KeyboardAvoidingView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Form from "./Component/Form";
import { signup } from "../../state-management/actions/auth/FirebaseAuthActions";
import { connect } from "react-redux";

const InstructorSignup = (props) => {
  const [loading, setLoading] = useState(false);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  const onFormSubmit = (data) => {
    setLoading(true);
    props.signup(data, setLoading);
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/TrainerPic.png")}
        style={styles.PicWrapper}
      >
        <View style={styles.Layer}></View>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
        >
          <ScrollView>
            <View style={styles.LogoBody}>
              <Image
                style={{ height: "70%", width: "70%" }}
                source={require("../../assets/logo.png")}
                resizeMode="cover"
              />
            </View>

            <View style={styles.formWrapper}>
              <Form
                onSubmit={(data) => onFormSubmit(data)}
                onLoginPress={() => props.navigation.navigate("Login")}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: hp("100%"),
    width: wp("100%"),
    backgroundColor: "#fff",
  },
  termsText: {
    fontWeight: "600",
    fontSize: rf(11),
    color: "#fff",
    marginLeft: "5%",
  },
  PicWrapper: {
    height: hp("100%"),
    width: wp("100%"),
    paddingTop:
      Platform.OS == "ios"
        ? StatusBar.currentHeight + hp("5%")
        : StatusBar.currentHeight + hp("1%"),
  },
  Layer: {
    height: hp("100%"),
    width: wp("100%"),
    opacity: 0.3,
    backgroundColor: "#E2B269",
    position: "absolute",
  },
  LogoWrapper: {
    height: hp("27%"),
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    justifyContent: "flex-end",
  },
  LogoBody: {
    height: hp("15%"),
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  TermsTextWrapper: {
    alignItems: "center",
    paddingHorizontal: wp("5%"),
    justifyContent: "space-evenly",
  },
  TermsBody: {
    width: "100%",
    paddingHorizontal: wp("10%"),
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  AccountLoginBody: {
    width: "100%",
    paddingHorizontal: "10%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 10,
  },
  formWrapper: {
    alignItems: "center",
    justifyContent: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_details: state.main.signup_details,
});
export default connect(mapStateToProps, { signup })(InstructorSignup);
