import React, { useState, useEffect } from "react";
import { Provider } from "react-redux";
import { AuthNavigator } from "./routes/AuthNavigator";
import store from "./state-management/store";

import * as firebase from "firebase";
import { firebaseConfig } from "./state-management/actions/env";
import { InstructorAppNavigator } from "./routes/InstructorAppNavigator";
import { VenueAppNavigator } from "./routes/VenueAppNavigator";
import { ActivityIndicator, View } from "react-native";
import { useFonts } from "expo-font";
import { AdminNavigator } from "./routes/AdminNavigator";
export default function App() {
  let [fontsLoaded] = useFonts({
    MB: require("./assets/fonts/Montserrat-Bold.ttf"),
    MM: require("./assets/fonts/Montserrat-Medium.ttf"),
    MR: require("./assets/fonts/Montserrat-Regular.ttf"),
    MS: require("./assets/fonts/Montserrat-SemiBold.ttf"),
  });
  const [role, setRole] = useState("loggedOut");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }

    try {
      firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          var db = firebase.firestore();
          setLoading(true);
          db.collection("users")
            .where("email", "==", user.providerData[0].email)
            .get()
            .then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                setLoading(false);
                setRole(doc.data().role);
                console.log(doc.data().role)
              });
            })
            .catch((error) => {
              console.log("Error getting documents: ", error);
              setLoading(false);
            });
           
        } else {
          setRole("LoggedOut");
          setLoading(false);
        }
      });
    } catch (error) {
      console.log(error.message);
      setLoading(false);
    }
  }, []);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="green" />
      </View>
    );
  }
  if (!fontsLoaded) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <Provider store={store}>
      {role == "venue" ? (
        <VenueAppNavigator />
      ) : role == "instructor" ? (
        <InstructorAppNavigator />
      ): role == "admin" ? (
        <AdminNavigator />
      ) : (
        <AuthNavigator />
      )}
    </Provider>
  );
}
