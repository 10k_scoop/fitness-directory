import {
  DO_LOGIN,
  DO_LOGOUT,
  DO_SIGNUP,
  GET_JOBS,
  VIEW_JOB,
  POST_JOB,
  GET_USER_DETAILS,
  MY_JOBS,
  MY_APPLICATIONS,
  APPLY_TO_JOB,
  UPDATEJOB,
  APPLIEDJOB,
  GET_ALL_USERS,
  GET_ALL_PAYMENTS,
  GET_ALL_NOTIFICATIONS,
  POST_REQUEST_PAYMENT,
  GET_ALL_REQUEST_PAYMENTS
} from "../types/types";
const initialState = {
  login_details: null,
  logout: null,
  signup_details: null,
  post_job: null,
  view_job: null,
  get_jobs: null,
  get_user_details: null,
  my_jobs: null,
  my_applications: null,
  apply_to_job: null,
  update_job: null,
  applied_jobs: null,
  get_all_users: null,
  get_all_payments: null,
  get_all_notifications: null,
  post_request_payment: null,
  get_all_request_payments:null
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case DO_LOGIN:
      return {
        ...state,
        login_details: action.payload,
      };
    case DO_LOGOUT:
      return {
        ...state,
        logout: action.payload,
      };
    case DO_SIGNUP:
      return {
        ...state,
        signup_details: action.payload,
      };
    case GET_JOBS:
      return {
        ...state,
        get_jobs: action.payload,
      };
    case POST_JOB:
      return {
        ...state,
        post_job: action.payload,
      };
    case VIEW_JOB:
      return {
        ...state,
        view_job: action.payload,
      };
    case GET_USER_DETAILS:
      return {
        ...state,
        get_user_details: action.payload,
      };
    case MY_JOBS:
      return {
        ...state,
        my_jobs: action.payload,
      };
    case MY_APPLICATIONS:
      return {
        ...state,
        my_applications: action.payload,
      };
    case APPLY_TO_JOB:
      return {
        ...state,
        apply_to_job: action.payload,
      };
    case UPDATEJOB:
      return {
        ...state,
        update_job: action.payload,
      };
    case APPLIEDJOB:
      return {
        ...state,
        applied_jobs: action.payload,
      };
    case GET_ALL_USERS:
      return {
        ...state,
        get_all_users: action.payload,
      };
    case GET_ALL_PAYMENTS:
      return {
        ...state,
        get_all_payments: action.payload,
      };
    case GET_ALL_NOTIFICATIONS:
      return {
        ...state,
        get_all_notifications: action.payload,
      };
    case POST_REQUEST_PAYMENT:
      return {
        ...state,
        post_request_payment: action.payload,
      };
    case GET_ALL_REQUEST_PAYMENTS:
      return {
        ...state,
        get_all_request_payments: action.payload,
      };
    default:
      return state;
  }
};
export default mainReducer;
