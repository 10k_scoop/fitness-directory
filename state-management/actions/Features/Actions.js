import {
  POST_JOB,
  GET_ERRORS,
  GET_JOBS,
  MY_JOBS,
  VIEW_JOB,
  MY_APPLICATIONS,
  APPLY_TO_JOB,
  UPDATEJOB,
  APPLIEDJOB,
  GET_ALL_USERS,
  GET_ALL_PAYMENTS,
  GET_ALL_NOTIFICATIONS,
  POST_REQUEST_PAYMENT,
  GET_ALL_REQUEST_PAYMENTS,
} from "../../types/types";
import * as firebase from "firebase";

export const getJobs = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs").onSnapshot((querySnapshot) => {
      var jobs = [];
      querySnapshot.forEach((doc) => {
        let arr = doc.data();
        arr.id = doc.id;
        jobs.push(arr);
      });
      dispatch({ type: GET_JOBS, payload: jobs });
      setLoading(false);
    });
  } catch (e) {
    console.log(e.message);
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const viewJob = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    var docRef = db.collection("jobs").doc(id);

    docRef
      .get()
      .then((doc) => {
        if (doc.exists) {
          console.log("Document data:", doc.data());
          dispatch({ type: VIEW_JOB, payload: doc.data() });
        }
      })
      .catch((error) => {
        console.log("Error getting document:", error);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const searchJob = (skill, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .where("skills", "array-contains", skill)
      .get()
      .then((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          let arr = doc.data();
          arr.id = doc.id;
          jobs.push(arr);
        });
        dispatch({ type: GET_JOBS, payload: jobs });
        setLoading(false);
      })
      .catch((error) => {
        console.log("Error getting documents: ", error);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const postJob = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .add(data)
      .then((docRef) => {
        dispatch({ type: POST_JOB, payload: "Job Posted Succesfully" });
        setLoading(false);
        alert("Congrats Your job is live now");
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log("empty");
  }
};
export const myJobs = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .where("author", "==", email)
      .onSnapshot((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          jobs.push(doc.data());
        });
        dispatch({ type: MY_JOBS, payload: jobs });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};
export const appliedJobs = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .where("applicants", "array-contains", email)
      .onSnapshot((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          jobs.push(doc.data());
        });
        dispatch({ type: APPLIEDJOB, payload: jobs });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const myApplications = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("applications")
      .where("author", "==", email)
      .onSnapshot((querySnapshot) => {
        var applications = [];
        querySnapshot.forEach((doc) => {
          applications.push({ data: doc.data(), id: doc.id });
        });
        dispatch({ type: MY_APPLICATIONS, payload: applications });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const applyToJob =
  (data, setLoading, setSubmitted) => async (dispatch) => {
    var db = firebase.firestore();
    let notiData = {
      createdAt: new Date(),
      from: data?.applicantEmail,
      to: data?.author,
      message: "one Candidate applied to you job check now !",
      type: "applied",
    };
    try {
      if (data.applicantDocs) {
        const response = await fetch(data.applicantDocs);
        const blob = await response.blob();
        var imgName = data.applicant + data.jobId;
        let imageRef = firebase
          .storage()
          .ref("images/applications/" + imgName + ".jpg");
        imageRef
          .put(blob)
          .then((snapshot) => {
            //You can check the image is now uploaded in the storage bucket
            imageRef
              .getDownloadURL()
              .then((url) => {
                data.applicantDocs = url;
                db.collection("applications")
                  .add(data)
                  .then(function (res) {
                    db.collection("jobs")
                      .doc(data.jobId)
                      .update({
                        applicants: data.applicants,
                      })
                      .then(function (res) {
                        db.collection("notifications")
                          .add(notiData)
                          .then(function (res) {
                            dispatch({
                              type: APPLY_TO_JOB,
                              payload: "Applied to job",
                            });
                            setLoading(false);
                            setSubmitted(true);
                          })
                          .catch((e) => {
                            setLoading(false);
                            console.log(e.message);
                          });
                      })
                      .catch((e) => {
                        dispatch({ type: GET_ERRORS, payload: e.message });
                        setLoading(false);
                      });
                  })
                  .catch((e) => {
                    dispatch({ type: GET_ERRORS, payload: e.message });
                    setLoading(false);
                  });
              })
              .catch((e) =>
                console.log("getting downloadURL of image error => ", e)
              );
          })
          .catch((e) => console.log("uploading image error => ", e));
      } else {
        db.collection("applications")
          .add(data)
          .then(function (res) {
            db.collection("jobs")
              .doc(data.jobId)
              .update({
                applicants: data.applicants,
              })
              .then(function (res) {
                db.collection("notifications")
                  .add(notiData)
                  .then(function (res) {
                    dispatch({
                      type: APPLY_TO_JOB,
                      payload: "Applied to job",
                    });
                    setLoading(false);
                    setSubmitted(true);
                  })
                  .catch((e) => {
                    setLoading(false);
                    console.log(e.message);
                  });
              })
              .catch((e) => {
                dispatch({ type: GET_ERRORS, payload: e.message });
                setLoading(false);
              });
          })
          .catch((e) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
          });
      }
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      console.log(e.message);
    }
  };

export const updateJob = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .doc(data.id)
      .update(data)
      .then(function (res) {
        dispatch({ type: UPDATEJOB, payload: "Updated" });
        setLoading(false);
        alert("Updated");
      })
      .catch((e) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const addJobToFavourites =
  (id, author, applicants, user, setLoading) => async (dispatch) => {
    var db = firebase.firestore();
    let data = {
      jobId: id,
      author: author,
      applicants: applicants,
      instructor: user,
    };

    try {
      db.collection("favourites")
        .add(data)
        .then(function (res) {
          setLoading(false);
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoading(false);
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      console.log(e.message);
    }
  };

export const updateFavourite = (data) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .doc(data.id)
      .update(data)
      .then(function (res) {})
      .catch((e) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    console.log(e.message);
  }
};

export const addSkills = (data, id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    var doc = db.collection("users").doc(id);
    return doc
      .update({
        skills: data,
      })
      .then(() => {
        setLoading(false);
        alert("Skills Updated");
      })
      .catch((e) => {
        setLoading(false);
        console.log(e.message);
      });
  } catch (e) {
    setLoading(false);
    console.log(e.message);
  }
};

export const approveUser = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("users")
      .where("email", "==", email)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          let id = doc.id;
          db.collection("users")
            .doc(id)
            .update({
              accountStatus: "approved",
            })
            .then(() => {
              setLoading(false);
              alert("Account Approved");
            })
            .catch((e) => {
              setLoading(false);
              console.log(e.message);
            });
        });
      })
      .catch((e) => {
        setLoading(false);
        console.log(e.message);
      });
  } catch (e) {
    setLoading(false);
    console.log(e.message);
  }
};

export const rejectUser =
  (email, setLoading, navigation) => async (dispatch) => {
    var db = firebase.firestore();
    try {
      db.collection("users")
        .where("email", "==", email)
        .get()
        .then(async(querySnapshot) => {
          let id = "";
          await querySnapshot.forEach((doc) => {
            id = doc.id;
          });
          await db.collection("users")
            .doc(id)
            .delete()
            .then(() => {
              console.log("Document successfully deleted!");
              navigation?.goBack();
            })
            .catch((error) => {
              setLoading(false);
              console.log(e.message);
            });
        })
        .catch((e) => {
          setLoading(false);
          console.log(e.message);
        });
    } catch (e) {
      setLoading(false);
      console.log(e.message);
    }
  };

export const updateBank = (data, id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    var doc = db.collection("users").doc(id);
    return doc
      .update({
        bankName: data.bankName,
        accountName: data.accountName,
        accountNumber: data.accountNumber,
      })
      .then(() => {
        setLoading(false);
        alert("Bank Updated");
      })
      .catch((e) => {
        setLoading(false);
        console.log(e.message);
      });
  } catch (e) {
    setLoading(false);
    alert(e.message);
  }
};

export const updateProfile = (data, id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    if (data.profile) {
      const response = await fetch(data.profile);
      const blob = await response.blob();
      let imageRef = firebase.storage().ref("images/" + id + ".jpg");
      imageRef
        .put(blob)
        .then((snapshot) => {
          //You can check the image is now uploaded in the storage bucket
          imageRef
            .getDownloadURL()
            .then((url) => {
              var doc = db.collection("users").doc(id);
              return doc
                .update({
                  name: data.name,
                  contact: data.contact,
                  profile: url,
                })
                .then(() => {
                  setLoading(false);
                  alert("Profile Updated");
                })
                .catch((e) => {
                  setLoading(false);
                  console.log(e.message);
                });
            })
            .catch((e) =>
              console.log("getting downloadURL of image error => ", e)
            );
        })
        .catch((e) => console.log("uploading image error => ", e));
    } else {
      var doc = db.collection("users").doc(id);
      return doc
        .update({
          name: data.name,
          contact: data.contact,
        })
        .then(() => {
          setLoading(false);
          alert("Profile Updated");
        })
        .catch((e) => {
          setLoading(false);
          console.log(e.message);
        });
    }
  } catch (e) {
    setLoading(false);
    console.log(e.message);
  }
};

export const uploadDocument = () => async (dispatch) => {};

export const getAllUsers = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("users").onSnapshot((querySnapshot) => {
      var users = [];
      querySnapshot.forEach((doc) => {
        users.push(doc.data());
        console.log(doc.id);
      });
      dispatch({ type: GET_ALL_USERS, payload: users });
      setLoading(false);
    });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const addPaymentDetailsToFirebase =
  (data, setLoader) => async (dispatch) => {
    let notiData = {
      createdAt: new Date(),
      to: "admin@gmail.com",
      from: data?.data?.recruiterEmail,
      message:
        data?.data?.recruiterEmail +
        " has made payment of £" +
        data?.data?.total,
      type: "paid",
    };
    let instructorNotiData = {
      createdAt: new Date(),
      to: data?.data?.applicantDetails?.email,
      from: data?.data?.recruiterEmail,
      message: "Congrats! " + data?.data?.recruiterEmail + " has hired you. ",
      type: "hired",
    };
    try {
      var db = firebase.firestore();
      db.collection("payments")
        .add(data)
        .then(function (res) {
          db.collection("users")
            .doc(data?.data?.applicantId)
            .update({
              hired: true,
              hiredByEmail: data?.data?.recruiterEmail,
              hiredById: data?.data?.recruiterId,
            })
            .then(() => {
              db.collection("jobs")
                .doc(data?.data?.jobId)
                .update({
                  hiredSomeone: true,
                })
                .then(() => {
                  db.collection("applications")
                    .doc(data?.data?.applicationId)
                    .update({
                      hiredByEmail: data?.data?.recruiterEmail,
                    })
                    .then(() => {
                      db.collection("notifications")
                        .add(notiData)
                        .then(function (res) {
                          db.collection("notifications")
                            .add(instructorNotiData)
                            .then(function (res) {
                              setLoader(false);
                              return true;
                            })
                            .catch((e) => {
                              setLoading(false);
                              console.log(e.message);
                            });
                        })
                        .catch((e) => {
                          setLoading(false);
                          console.log(e.message);
                        });
                    })
                    .catch((e) => {
                      setLoading(false);
                      console.log(e.message);
                    });
                })
                .catch((e) => {
                  setLoading(false);
                  console.log(e.message);
                });
            })
            .catch((e) => {
              setLoading(false);
              console.log(e.message);
            });
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoader(false);
          console.log("inner catch");
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoader(false);
      console.log("last catch");
    }
  };

export const getAllPayments = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("payments").onSnapshot((querySnapshot) => {
      var payments = [];
      querySnapshot.forEach((doc) => {
        payments.push({ data: doc.data(), id: doc.id });
      });
      dispatch({ type: GET_ALL_PAYMENTS, payload: payments });
      setLoading(false);
    });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const getAllNotifications = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("notifications").onSnapshot((querySnapshot) => {
      var noti = [];
      querySnapshot.forEach((doc) => {
        noti.push({ data: doc.data(), id: doc.id });
      });
      dispatch({ type: GET_ALL_NOTIFICATIONS, payload: noti });
      setLoading(false);
    });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const requestNewPayment = (prevData, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  let notiData = {
    createdAt: new Date(),
    to: prevData?.recruiterEmail,
    from: "admin@gmail.com",
    message:
      "Hello ! Your weekly hiring fee renewal is here! please click to pay. ",
    type: "payment",
  };
  try {
    db.collection("requestPayments")
      .add(prevData)
      .then(function (res) {
        db.collection("notifications")
          .add(notiData)
          .then(function (res) {
            setLoading(false);
            dispatch({ type: POST_REQUEST_PAYMENT, payload: res });
            alert("Payment request sent.");
          })
          .catch((e) => {
            setLoading(false);
            console.log(e.message);
            alert("Sorry! something went wrong");
          });
      })
      .catch((e) => {
        setLoading(false);
        alert("Sorry! something went wrong");
      });
  } catch (e) {
    console.log(e.message);
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const getRequestPayments = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("requestPayments").onSnapshot((querySnapshot) => {
      var req = [];
      querySnapshot.forEach((doc) => {
        req.push({ data: doc.data(), id: doc.id });
      });
      dispatch({ type: GET_ALL_REQUEST_PAYMENTS, payload: req });
      setLoading(false);
    });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const addRequestPaymentDetailsToFirebase =
  (data, setLoader, removalData) => async (dispatch) => {
    let notiData = {
      createdAt: new Date(),
      to: "admin@gmail.com",
      from: data?.data?.recruiterEmail,
      message:
        data?.data?.recruiterEmail +
        " has made payment of £" +
        data?.data?.total,
      type: "paid",
    };
    let instructorNotiData = {
      createdAt: new Date(),
      to: data?.data?.applicantDetails?.email,
      from: data?.data?.recruiterEmail,
      message: "Congrats! " + data?.data?.recruiterEmail + " has hired you. ",
      type: "hired",
    };
    try {
      var db = firebase.firestore();
      db.collection("payments")
        .add(data)
        .then(function (res) {
          db.collection("users")
            .doc(data?.data?.applicantId)
            .update({
              hired: true,
              hiredByEmail: data?.data?.recruiterEmail,
              hiredById: data?.data?.recruiterId,
            })
            .then(() => {
              db.collection("jobs")
                .doc(data?.data?.jobId)
                .update({
                  hiredSomeone: true,
                })
                .then(() => {
                  db.collection("applications")
                    .doc(data?.data?.applicationId)
                    .update({
                      hiredByEmail: data?.data?.recruiterEmail,
                    })
                    .then(() => {
                      db.collection("notifications")
                        .add(notiData)
                        .then(function (res) {
                          db.collection("notifications")
                            .add(instructorNotiData)
                            .then(function (res) {
                              db.collection("notifications")
                                .doc(removalData?.notiId)
                                .delete()
                                .then(() => {
                                  db.collection("requestPayments")
                                    .doc(removalData?.reqPaymentId)
                                    .delete()
                                    .then(() => {
                                      setLoader(false);
                                      return true;
                                    })
                                    .catch((e) => {
                                      setLoading(false);
                                      console.log(e.message);
                                    });
                                })
                                .catch((e) => {
                                  setLoading(false);
                                  console.log(e.message);
                                });
                            })
                            .catch((e) => {
                              setLoading(false);
                              console.log(e.message);
                            });
                        })
                        .catch((e) => {
                          setLoading(false);
                          console.log(e.message);
                        });
                    })
                    .catch((e) => {
                      setLoading(false);
                      console.log(e.message);
                    });
                })
                .catch((e) => {
                  setLoading(false);
                  console.log(e.message);
                });
            })
            .catch((e) => {
              setLoading(false);
              console.log(e.message);
            });
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoader(false);
          console.log("inner catch");
        });
    } catch (e) {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoader(false);
      console.log("last catch");
    }
  };
