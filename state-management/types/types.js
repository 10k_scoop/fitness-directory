export const GET_ERRORS = "GET_ERRORS";
export const DO_LOGIN = "DO_LOGIN";
export const DO_LOGOUT = "DO_LOGOUT";
export const DO_SIGNUP = "DO_SIGNUP";
export const GET_JOBS = "GET_JOBS";
export const POST_JOB = "POST_JOB";
export const VIEW_JOB = "VIEW_JOB";
export const MY_JOBS = "MY_JOBS";
export const GET_USER_DETAILS = "GET_USER_DETAILS";
export const MY_APPLICATIONS = "MY_APPLICATIONS";
export const APPLY_TO_JOB = "APPLY_TO_JOB";
export const UPDATEJOB = "UPDATEJOB";
export const APPLIEDJOB = "APPLIEDJOB";
export const GET_ALL_USERS = "GET_ALL_USERS";
export const GET_ALL_PAYMENTS = "GET_ALL_PAYMENTS";
export const GET_ALL_NOTIFICATIONS = "GET_ALL_NOTIFICATIONS";
export const GET_ALL_REQUEST_PAYMENTS = "GET_ALL_REQUEST_PAYMENTS";
export const POST_REQUEST_PAYMENT = "POST_REQUEST_PAYMENT";
