import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AdminLogin from "../screens/Admin-Login/AdminLogin";
import AdminDashboard from "../screens/Admin-Dashboard/AdminDashboard";
import AdminSettings from "../screens/Admin-Settings/AdminSettings";
import AdminApprovals from "../screens/Admin-Approvals/AdminApprovals";
import ViewApproval from "../screens/View-Approval/ViewApproval";
import ViewUserProfile from "../screens/ViewUserProfile/ViewUserProfile";
import AdminNotifications from "../screens/AdminNotifications";
import PaymentRecord from "../screens/Admin-PaymentsRecord/PaymentRecord";
import ViewSinglePayment from "../screens/ViewSinglePayment/ViewSinglePayment";
const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
    <Navigator headerMode="none">
        <Screen name="AdminDashboard" component={AdminDashboard} />
        <Screen name="ViewApproval" component={ViewApproval} />
        <Screen name="AdminApprovals" component={AdminApprovals} />
        <Screen name="AdminSettings" component={AdminSettings} />
        <Screen name="ViewUserProfile" component={ViewUserProfile} />
        <Screen name="AdminNotifications" component={AdminNotifications} />
        <Screen name="PaymentRecord" component={PaymentRecord} />
        <Screen name="ViewSinglePayment" component={ViewSinglePayment} />
    </Navigator>
);

export const AdminNavigator = () => (
    <NavigationContainer>
        <HomeNavigator />
    </NavigationContainer>
);
