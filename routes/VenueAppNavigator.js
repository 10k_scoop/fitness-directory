import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Notifications from "../screens/Notifications";
import VenueDashboard from "../screens/Venue-Dashboard/VenueDashboard";
import VenueJob from "../screens/Venue-Jobs/VenueJob";
import VenueProfile from "../screens/VenueProfile/VenueProfile";
import PostJob from "../screens/PostJob/PostJob";
import Myjobs from "../screens/Myjobs";
import Applicants from "../screens/Applicants/Applicants";
import VenueSettings from "../screens/VenueSettings/VenueSettings";
import ChangePassword from "../screens/ChangePassword/ChangePassword";
import SingleApplication from "../screens/SingleApplication/SingleApplication";
import EditJob from "../screens/EditJob/EditJob";
import PaymentMethod from "../screens/PaymentMethod/PaymentMethod";
import VenueNotification from "../screens/VenueNotification";
import RequestedPayments from "../screens/RequestedPayments/RequestedPayments";
import ViewRequestPaymentDetail from "../screens/ViewRequestPaymentDetail/ViewRequestPaymentDetail";
import PayRequestPaymentMethod from "../screens/PayRequestPaymentMethod/PayRequestPaymentMethod";
import VenueBankDetails from "../screens/VenueBankDetails/VenueBankDetails";

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator headerMode="none">
    <Screen name="VenueDashboard" component={VenueDashboard} />
    <Screen name="VenueProfile" component={VenueProfile} />
    <Screen name="PostJob" component={PostJob} />
    <Screen name="VenueNotification" component={VenueNotification} />
    <Screen name="VenueJob" component={VenueJob} />
    <Screen name="Myjobs" component={Myjobs} />
    <Screen name="VenueSettings" component={VenueSettings} />
    <Screen name="ChangePassword" component={ChangePassword} />
    <Screen name="applicants" component={Applicants} />
    <Screen name="SingleApplication" component={SingleApplication} />
    <Screen name="EditJob" component={EditJob} />
    <Screen name="PaymentMethod" component={PaymentMethod} />
    <Screen name="PayRequestPaymentMethod" component={PayRequestPaymentMethod} />
    <Screen name="RequestedPayments" component={RequestedPayments} />
    <Screen name="ViewRequestPaymentDetail" component={ViewRequestPaymentDetail} />
    <Screen name="VenueBankDetails" component={VenueBankDetails} />

  </Navigator>
);

export const VenueAppNavigator = () => (
  <NavigationContainer>
    <HomeNavigator />
  </NavigationContainer>
);
