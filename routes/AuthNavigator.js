import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import InstructorSignup from "../screens/InstructorSignup/InstructorSignup";
import Login from "../screens/Login/Login";
import VenueSignup from "../screens/VenueSignup/VenueSignup";
import ChooseOption from "../screens/ChooseOption.js/ChooseOption";
import ForgotPassword from "../screens/ForgotPassword/ForgotPassword";

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator headerMode="none">
    <Screen name="ChooseOption" component={ChooseOption} />
    <Screen name="Login" component={Login} />
    <Screen name="VenueSignup" component={VenueSignup} />
    <Screen name="InstructorSignup" component={InstructorSignup} />
    <Screen name="ForgotPassword" component={ForgotPassword} />
  </Navigator>
);

export const AuthNavigator = () => (
  <NavigationContainer>
    <HomeNavigator />
  </NavigationContainer>
);
