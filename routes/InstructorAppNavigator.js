import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import InstructorDashboard from "../screens/InstructorDashboard";
import Myjobs from "../screens/Myjobs";
import Notifications from "../screens/Notifications";
import SearchJobs from "../screens/SearchJobs";
import ViewJob from "../screens/View-Jobs/ViewJob";
import ApplyToJob from "../screens/ApplyToJob/ApplyToJob";
import AppliedSuccesfull from "../screens/AppliedSuccesfull";
import Profile from "../screens/Profile/Profile";
import Favourites from "../screens/Favourites";
import InstructorSettings from "../screens/InstructorSettings/InstructorSettings";
import InstructorSkills from "../screens/InstructorSkills/InstructorSkills";
import ChangePassword from "../screens/ChangePassword/ChangePassword";
import AppliedJobs from "../screens/AppliedJobs/AppliedJobs";
import InstructorBankDetails from "../screens/InstructorBankDetails/InstructorBankDetails";

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator headerMode="none">
    <Screen name="InstructorDashboard" component={InstructorDashboard} />
    <Screen name="Profile" component={Profile} />
    <Screen name="Notifications" component={Notifications} />
    <Screen name="Favourites" component={Favourites} />
    <Screen name="AppliedJobs" component={AppliedJobs} />
    <Screen name="SearchJobs" component={SearchJobs} />
    <Screen name="ViewJob" component={ViewJob} />
    <Screen name="ApplyToJob" component={ApplyToJob} />
    <Screen name="AppliedSuccesfull" component={AppliedSuccesfull} />
    <Screen name="InstructorSettings" component={InstructorSettings} />
    <Screen name="InstructorSkills" component={InstructorSkills} />
    <Screen name="ChangePassword" component={ChangePassword} />
    <Screen name="InstructorBankDetails" component={InstructorBankDetails} />
  </Navigator>
);

export const InstructorAppNavigator = () => (
  <NavigationContainer>
    <HomeNavigator />
  </NavigationContainer>
);
