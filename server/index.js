import express from "express";
import cors from "cors";

const app = express();

const PUBLISHABLE_KEY = "pk_test_51GubA8LrQYi8DtCUhSYrFSWKhbzQ2q0wO5A1Q1izE92vVhmMPQa5eOiOmWO5si2mS3nj7yHmubMfHq2Q4T3xTsQD003fPTZqMp";
const SECRET_KEY =
  "sk_test_51GubA8LrQYi8DtCUtUaFz4PR22ooXuT9Uf9rUiaoT7Y8m8V7nimDWeDt3OSd8C4kPEUPaJvcgnLEajvjJUdlgw8G00u6yM9G5C";
import Stripe from "stripe";

//Confirm the API version from your stripe dashboard
app.use(express.json());
app.use(cors());
const stripe = Stripe(SECRET_KEY, { apiVersion: "2020-08-27" });

const port = process.env.PORT || 3000;
//const port = 3000;
app.listen(port);

app.post("/create-payment-intent", async (req, res) => {
  try {
    let data=req.body.data
    const paymentIntent = await stripe.paymentIntents.create({
      amount: data.total, //lowest denomination of particular currency
      currency: "gbp",
      receipt_email:data.recruiterEmail,
      payment_method_types: ["card"], //by default
    });

    const clientSecret = paymentIntent.client_secret;

    res.json({
      clientSecret: clientSecret,
    });
  } catch (e) {
    console.log(e.message);
    res.json({ error: e.message });
  }
});
